# base image
FROM nginx:1.17.8

# copy sites to nginx
COPY ./sites /usr/share/nginx/html

# copy conf.d to nginx
COPY ./conf.d /etc/nginx

EXPOSE 80
EXPOSE 443
