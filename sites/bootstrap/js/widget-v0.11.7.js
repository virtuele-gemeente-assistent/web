const script = document.getElementById("widget-script");

// Determine socket URL from the src used to include the widget script
const scriptUrl = script.src.split("/");
let domain = scriptUrl[2];
const baseUrl = scriptUrl[0] + "//" + domain + "/";

// Load Sentry
if(domain === "virtuele-gemeente-assistent.nl") {
    var sentryScript = document.createElement('script');
    sentryScript.onload = function () {
        Sentry.init({
            dsn: 'https://b8c04895de184d84948d95fce57239f1@virtuele-gemeente-assistent.nl/283',
            allowUrls: [
                /.*\/static\/js\/widget.*\.js/,
                /.*\/static\/js\/obi_livechat.*\.js/,
                /.*\/static\/js\/webchat.*\.js/
            ],
        });
    };
    sentryScript.src = `${baseUrl}static/js/sentry-6.13.2.js`;

    document.head.appendChild(sentryScript);
}

// Defaults for custom attributes
let customAttrs = {
    gemEnabled: true,
    directHandover: false,
    widgetTitle: "Gem",
    widgetSubtitle: "Je digitale hulp van de gemeente",
    popupMessage: "Stel je vraag",
    avatarUrl: `${baseUrl}/static/img/avatar.png`,
}

let allowEmpty = {
    gemEnabled: true,
    directHandover: true,
    widgetTitle: true,
    widgetSubtitle: true,
    popupMessage: true,
    avatarUrl: false,
}

// Override default attrs with values from widget attributes
for(const prop in script.dataset) {
    if (!customAttrs.hasOwnProperty(prop)) continue;

    let overrideValue = script.dataset[prop];
    if (typeof(customAttrs[prop]) === "boolean") {
        if(overrideValue === "true") overrideValue = true;
        else overrideValue = false;
    }
    customAttrs[prop] = overrideValue;
}

// TODO generic handler for this
// Retrieve the custom data
const municipality = script.dataset.municipality;
const organisationSlug = script.dataset.organisationSlug;
const nlxOrganizationName = script.dataset.nlxOrgName;
const nlxDemo = script.dataset.nlxDemo;
const clientName = script.dataset.clientName;
const classPrefix = "rw";

let initPayload = "/greet";
if(municipality) {
    initPayload = `/greet{\"municipality\":\"${municipality}\"}`
}


let customData = {};

// Redirected from another municipality/service
let params = new URLSearchParams(window.location.search)
if(window.location.search.includes("redirect_conv")) {
    let params = new URLSearchParams(window.location.search)
    customData.convLoc = params.get("conv_loc");
    customData.token = params.get("token");
    initPayload = "/transfer"
}

const useRouter = script.dataset.useRouter ? true : false;

// Attributes to test external services
const customService = script.dataset.customService;
const routerInitPayload = script.dataset.initPayload;
const recipientId = script.dataset.recipientId;

let socketPath = "/socket.io";
let onSocketEvent;

let openChatLabel = "Open chatwidget";
let closeChatLabel = "Sluit chatwidget";

/**
 ****************************************************************
 * Utility functions
 ****************************************************************
 */

// Source: https://stackoverflow.com/a/56399194
var waitUntilElementExists = (selector, callback, timeout=500) => {
    const el = document.querySelector(selector);
    if (el) {
        return callback(el);
    }

    setTimeout(() => waitUntilElementExists(selector, callback), timeout);
}


var waitUntilElementNotExists = (selector, callback) => {
    const el = document.querySelector(selector);
    if (!el) {
        return callback(el);
    }

    setTimeout(() => waitUntilElementNotExists(selector, callback), 500);
}


function isVisible(e) {
    return !!( e.offsetWidth || e.offsetHeight || e.getClientRects().length );
}

/**
 ****************************************************************
 * Script functionality (for widget configuration)
 ****************************************************************
 */

/**
 * Set the username as custom data for Rasa
 */
if(clientName) {
    customData.username = clientName;
} else {
    const queryString = window.location.search;
    if(queryString) {
        const urlParams = new URLSearchParams(queryString);
        const username = urlParams.get("username");
        if(username) customData.username = username;
    }
}


/**
 * Livechat integrations (OBI/Livecom)
 */
// Retrieve the livechat script
const livechat = document.getElementById("livechat-script");

if(municipality) {
    customData.municipality = municipality;

    if(nlxDemo) {
        customData.nlx_demo = true;
    }

    if(nlxOrganizationName) {
        customData.nlx_org_name = nlxOrganizationName;
    }
}

if(customService) customData.customService = customService;
if(customService) customData.routerInitPayload = routerInitPayload;
if(customService) customData.recipientId = recipientId;

if(
    window.location.href.includes("mijn.test.virtuele-gemeente-assistent.nl")
    || window.location.href.includes("mijn.virtuele-gemeente-assistent.nl")
) {
    customData.chatStartedOnPage = `#${municipality}`;
} else {
    customData.chatStartedOnPage = window.location.href;
}

let initializeLivechat = function(callback, attrs) { return undefined; }

if(livechat) {
    switch(livechat.dataset.name) {
        // Currently only supports obi4wan
        case "obi4wan":
            customData.livechat = livechat.dataset.name;
            customData.guid = livechat.dataset.guid;
            onSocketEvent = {
                'bot_uttered': (data) => {
                    if(data.history) {
                        // Store the municipality in the session, needed to restore
                        // the connection with Obi4Wan on page refresh
                        window.sessionStorage.setItem("municipality", data.municipality);

                        init(data.municipality, history=data.history);
                    } else if ((data.custom || {}).history) {  
                        // Temporary fix: https://gitlab.com/virtuele-gemeente-assistent/gem/-/issues/3918
                        // allow OBI livechat for router connections

                        // Store the municipality in the session, needed to restore
                        // the connection with Obi4Wan on page refresh
                        window.sessionStorage.setItem("municipality", data.custom.municipality);

                        init(data.custom.municipality, history=data.custom.history);
                    }
                },
            };
            initializeLivechat = async function(guid, callback, attrs) {
                // Skip the availability check if already in active livechat
                if (window.sessionStorage.getItem("municipality")) {
                    callback(attrs);
                    return
                }

                // TODO should be generic implementation in router?
                let availabilityUrl = `https://cloudstatic.obi4wan.com/api/v1.0/chat/availability/${guid}`
                fetch(availabilityUrl).then(response => response.json()).then(data => {
                    console.log("Retrieving livechat status from OBI4Wan");
                    console.log(data);
                    if(data.available) {
                        callback(attrs);
                    }
                })
            }
    }
} else {
    // Generic scenario
    initializeLivechat = async function(callback, attrs) {
        // Skip the availability check if already in active livechat
        if (window.sessionStorage.getItem("municipality")) {
            callback(attrs);
            return
        }

        window.sessionStorage.setItem("municipality", organisationSlug)

        // TODO should be generic implementation in router?
        let availabilityUrl = `${baseUrl}router/livechat-status`
        makeRequest(availabilityUrl, data={url: customData.chatStartedOnPage, organisation: organisationSlug}, method='POST')
        .then(response => response.json()).then(data => {
            console.log("Retrieving livechat status from adapter");
            console.log(data);
            if(data.status === "ok") {
                callback(attrs);
            }
        })
    }
}


/**
 * Experimental router functionality (currently not in use)
 */
if(
    ["Dordrecht", "Sliedrecht", "Alblasserdam", "Hendrik-Ido-Ambacht", "Zwijndrecht", "Zuid-Drecht", "Roosendaal", "Utrecht", "Rotterdam"].includes(municipality)
    || customService
    || useRouter
) {
    socketPath = "/router/socket.io";

    // TODO: should be implemented in botfront widget (https://gitlab.com/virtuele-gemeente-assistent/gem/-/issues/1387)
    // Workaround for custom avatars
    if(window.sessionStorage.getItem("avatarUrl")) {
        document.documentElement.style.setProperty("--avatar-url", window.sessionStorage.getItem("avatarUrl"));
        document.documentElement.style.setProperty("--display-override-avatar", window.sessionStorage.getItem("displayOverrideAvatar"));
    }

    let copiedOnSocketEvent = onSocketEvent
    onSocketEvent = {
        'bot_uttered': (data) => {
            // Also run OBI behaviour
            if(copiedOnSocketEvent) copiedOnSocketEvent.bot_uttered(data)

            if((data.metadata || {}).avatarUrl) {
                window.sessionStorage.setItem("avatarUrl", `url("${data.metadata.avatarUrl}")`);
                window.sessionStorage.setItem("displayOverrideAvatar", "block");
                document.documentElement.style.setProperty("--avatar-url", window.sessionStorage.getItem("avatarUrl"));
                document.documentElement.style.setProperty("--display-override-avatar", window.sessionStorage.getItem("displayOverrideAvatar"));
            } else {
                window.sessionStorage.removeItem("displayOverrideAvatar");
                document.documentElement.style.setProperty("--display-override-avatar", "none");
            }

            // TODO: should be implemented in botfront widget (https://gitlab.com/virtuele-gemeente-assistent/gem/-/issues/1387)
            if((data.metadata || {}).senderName) {
                window.sessionStorage.setItem("senderLabel", data.metadata.senderName);
            } else {
                window.sessionStorage.setItem("senderLabel", "Gem");
            }
        }
    };
} else {
    // TODO tempfix for Drechtsteden router livechat and regular livechat
    customData.skipRouter = true;
}


/**
 ****************************************************************
 * CUSTOM WIDGET IMPROVEMENTS
 ****************************************************************
 */


/**
 * Handles clicks on menu links, sends the clicked text as a user message and
 * closes the menu
 * @param {HTMLElement} linkElement 
 * @param {string} message 
 * @param {HTMLElement} dropdown 
 */
function sendUserMessageListener(linkElement, message, dropdown) {
    linkElement.addEventListener("click", function() {
        const textBar = document.getElementsByClassName(`${classPrefix}-new-message`)[0];
        textBar.value = message;
        const button = document.getElementsByClassName(`${classPrefix}-send`)[0]
        button.disabled = false;
        button.click();

        // Hide the menu again
        dropdown.classList.remove("show");

        // Set focus to text input
        document.querySelector(`.${classPrefix}-new-message`).focus();
    });
}


// TODO: should be implemented in botfront widget (https://gitlab.com/virtuele-gemeente-assistent/gem/-/issues/1387)
/**
 * Adds a menu to the widget
 */
function addMenu() {
    const header = document.getElementsByClassName(`${classPrefix}-header`)[0];

    const menu = document.createElement("div");
    menu.className = "gem-options-menu";
    menu.role = "menu";

    // Fade in menu
    if(menu.animate) {
        menu.animate([
                // keyframes
                { opacity: '0%' },
                { opacity: '100%' },
            ], {
            // timing options
            duration: 500,
            iterations: 1
        });
    }

    const button = document.createElement("button");
    button.setAttribute("aria-label", "Open menu");
    button.setAttribute("aria-controls", "gem-dropdown-menu");
    button.setAttribute("aria-haspopup", "true");
    button.id = "menuButton";
    button.className = "menu-button";
    button.setAttribute("aria-expanded", "false");
    button.innerHTML = "<div class=\"gem-menu-bar\"></div><div class=\"gem-menu-bar\"></div><div class=\"gem-menu-bar\"></div>";

    menu.appendChild(button);

    const dropdown = document.createElement("ul");

    dropdown.className = "gem-dropdown-menu";
    dropdown.id = "gem-dropdown-menu";

    const feedbackItem = document.createElement("li");
    const feedbackLink = document.createElement("button");
    feedbackLink.className = "gem-dropdown-item menu-button";
    feedbackLink.textContent = "Je mening geven";
    sendUserMessageListener(feedbackLink, "feedback", dropdown)
    feedbackItem.appendChild(feedbackLink)

    const downloadConversationItem = document.createElement("li");
    const downloadConversationLink = document.createElement("button");
    downloadConversationLink.className = "gem-dropdown-item menu-button";
    downloadConversationLink.textContent = "Download gesprek";
    sendUserMessageListener(downloadConversationLink, "download gesprek", dropdown)
    downloadConversationItem.appendChild(downloadConversationLink)

    const privacyItem = document.createElement("li");
    const privacyLink = document.createElement("button");
    privacyLink.className = "gem-dropdown-item menu-button";
    privacyLink.textContent = "Privacy";
    sendUserMessageListener(privacyLink, "privacy", dropdown)
    privacyItem.appendChild(privacyLink)

    const resetGemItem = document.createElement("li");
    const resetGemLink = document.createElement("button");
    resetGemLink.className = "gem-dropdown-item menu-button";
    resetGemLink.textContent = "Herstart chatsessie";
    resetGemLink.addEventListener("click", () => {
        if (window.sessionStorage) {
            window.sessionStorage.clear();
        }
        window.location = window.location;
    })
    resetGemItem.appendChild(resetGemLink)

    // Ignore top three items for livechat only widget
    if(initPayload !== "/direct_escalatie") {
        dropdown.appendChild(feedbackItem);
        dropdown.appendChild(downloadConversationItem);
        dropdown.appendChild(privacyItem);
    }
    dropdown.appendChild(resetGemItem);
    menu.appendChild(dropdown);
    header.appendChild(menu);

    button.addEventListener("click", function(e) {
        e.stopPropagation()
        if(dropdown.classList.contains("show")) {
            dropdown.classList.remove("show");
            button.setAttribute("aria-expanded", "false");
            button.setAttribute("aria-label", "Open menu");
        } else {
            dropdown.classList.add("show");
            button.setAttribute("aria-expanded", "true");
            button.setAttribute("aria-label", "Sluit menu");
        }
    });

}


// TODO: should be implemented in botfront widget (https://gitlab.com/virtuele-gemeente-assistent/gem/-/issues/1387)
/**
 * Adds extra labels/attrs to responses for accessibility purposes
 */
function addMessageAccessibilityProps() {
    let userMessages = document.querySelectorAll(`.${classPrefix}-from-client .${classPrefix}-message-text`);

    for(let i = 0; i < userMessages.length; i++) {
        userMessages[i].setAttribute("tabindex", "0");
        userMessages[i].setAttribute("aria-label", `U zegt: ${userMessages[i].textContent}`);
        userMessages[i].setAttribute("role", "dialog");
    }

    let senderLabel = window.sessionStorage.getItem("senderLabel") || "Gem";
    let botMessages = document.querySelectorAll(`.${classPrefix}-from-response .${classPrefix}-message .${classPrefix}-response`);
    for(let i = 0; i < botMessages.length; i++) {
        botMessages[i].setAttribute("tabindex", "0");
        botMessages[i].setAttribute("aria-label", `${senderLabel} zegt: ${botMessages[i].textContent}`);
        botMessages[i].setAttribute("role", "dialog");
    }

    let avatars = document.querySelectorAll(`.${classPrefix}-avatar`);
    for(let i = 0; i < avatars.length; i++) {
        if(avatars[i].parentElement.classList.contains(`${classPrefix}-typing-indication`)) {
            avatars[i].setAttribute("alt", `${senderLabel} is aan het typen...`);
        } else if(!avatars[i].getAttribute("alt") != "profile") {
            avatars[i].setAttribute("alt", `${senderLabel} zegt:`);
        }
    }

    let buttonsDiv = document.querySelector(`.${classPrefix}-replies`);
    if(buttonsDiv) buttonsDiv.setAttribute("aria-label", "Knoppen");

    let textInput = document.querySelector(`.${classPrefix}-new-message`);

    // Make buttons in chat clickable accessible for keyboard only users
    let buttons = document.querySelectorAll(`.${classPrefix}-reply`);
    for(let i = 0; i < buttons.length; i++) {
        buttons[i].setAttribute("tabindex", "0");
        buttons[i].setAttribute("aria-label", `Knop: ${buttons[i].textContent}`);
        buttons[i].setAttribute("role", "button");
        buttons[i].addEventListener("keypress", (e) => {
            if([" ", "Enter"].includes(e.key)) {
                e.target.click();
                textInput.focus();
            }
        });
    }
}


// TODO: should be implemented in botfront widget (https://gitlab.com/virtuele-gemeente-assistent/gem/-/issues/1387)
/**
 * Adds extra labels/attrs to responses for accessibility purposes whenever
 * new changes to the messages view occur
 */
function addMessageAccessibilityOnChange() {
    const observer = new MutationObserver(() => {
        addMessageAccessibilityProps()
    });

    // Start observing the target node for configured mutations
    observer.observe(document.querySelector(`#${classPrefix}-messages`), {childList: true});
}


// TODO: should be implemented in botfront widget (https://gitlab.com/virtuele-gemeente-assistent/gem/-/issues/1387)
/**
 * Adds the required attributes for accessibility to the widget
 */
function addWidgetAccessibilityProps() {
    document.querySelector(`#${classPrefix}-messages`).setAttribute("aria-live", "polite");
    document.querySelector(`#${classPrefix}-messages`).setAttribute("role", "log");
    document.querySelector(`.${classPrefix}-send`).setAttribute("aria-label", "Verstuur bericht");

    let textArea = document.querySelector(`.${classPrefix}-new-message`);
    textArea.setAttribute("aria-label", "Typ hier je vraag...");

    let closeButton = document.querySelector(`.${classPrefix}-launcher`);
    let menuButton = document.querySelector("#menuButton");

    let mobileCloseButton = document.querySelector(`.${classPrefix}-close-button`);
    mobileCloseButton.setAttribute("tabindex", "0");

    // Adjust widget button label if widget is closed with mobile close button
    mobileCloseButton.addEventListener("click", (e) => {
        closeButton.setAttribute("aria-label", openChatLabel);
    });

    // Configure tab order for mobile close button
    mobileCloseButton.addEventListener("keydown", (e) => {
        if(e.key === "Tab" && e.shiftKey) {
            e.preventDefault();
            textArea.focus()
        }
    });

    textArea.addEventListener("keydown", (e) => {
        if(e.key === "Tab" && !e.shiftKey) {
            e.preventDefault();
            if(isVisible(mobileCloseButton)) {
                mobileCloseButton.focus();
            } else {
                closeButton.focus();
            }
        }
    });

    // Custom tab order, let menu button receive focus after text input
    // Only if widget is open
    closeButton.addEventListener("keydown", (e) => {
        if(e.key === "Tab" && !e.shiftKey && WebChat.isOpen()) {
            e.preventDefault();
            menuButton.focus();
        }
    });

    // Shift + tab on menubutton returns focus to close button
    menuButton.addEventListener("keydown", (e) => {
        if(e.key === "Tab" && e.shiftKey) {
            e.preventDefault();
            if(isVisible(mobileCloseButton)) {
                mobileCloseButton.focus();
            } else {
                closeButton.focus();
            }
        }
    });
}


/**
 * Add the custom components on page load/on widget open
 */
function addWidgetProps() {
    waitUntilElementExists(`.${classPrefix}-launcher`, (el) => {
        const chatButton = document.getElementsByClassName(`${classPrefix}-launcher`)[0];
        // Change launcher label depending on state
        if(chatButton.classList.contains(`${classPrefix}-hide-sm`)) {
            chatButton.setAttribute("aria-label", closeChatLabel);
            document.querySelector(`.${classPrefix}-close-launcher`).setAttribute("alt", "Sluit chat icoon");
        } else {
            chatButton.setAttribute("aria-label", openChatLabel);
        }

        // If the widget is already opened
        waitUntilElementExists(`.${classPrefix}-header`, (el) => {
            addMenu();

            addMessageAccessibilityProps();
            addMessageAccessibilityOnChange();
            addWidgetAccessibilityProps();
        });

        // If the widget is (re)opened using the button
        chatButton.addEventListener("click", function() {
            waitUntilElementExists(`.${classPrefix}-header`, (el) => {
                addMenu();

                addMessageAccessibilityProps();
                addMessageAccessibilityOnChange();
                addWidgetAccessibilityProps();
            });

            // Change launcher label depending on state
            if(this.classList.contains(`${classPrefix}-hide-sm`)) {
                chatButton.setAttribute("aria-label", openChatLabel);
            } else {
                chatButton.setAttribute("aria-label", closeChatLabel);
                waitUntilElementExists(`.${classPrefix}-close-launcher`, (el) => {
                    el.alt = "Sluit chat icoon";
                });
            }
        });
    });
}


// TODO: should be implemented in botfront widget (https://gitlab.com/virtuele-gemeente-assistent/gem/-/issues/1387)
/**
 * Add indicator message for widget button (if not yet clicked away)
 */
function addWidgetIndicator(attrs) {
    if(!window.sessionStorage.getItem("indicatorShown")) {
        // Add a tooltip for a new session
        waitUntilElementExists(`.${classPrefix}-open-launcher__container`, (el) => {
            var launcher = document.getElementsByClassName(`${classPrefix}-open-launcher__container`)[0];
            launcher.innerHTML += `
                <div class="${classPrefix}-tooltip-body initial-msg" role="button" tabindex="0" aria-label="${attrs.popupMessage}">
                    <div class="${classPrefix}-tooltip-close">
                        <button>&#10005;</button>
                    </div>
                    <div class="${classPrefix}-tooltip-response initial-msg">
                        <div class="${classPrefix}-response">
                            <div class="${classPrefix}-message-text">
                                <div class="${classPrefix}-markdown">
                                    <p>${attrs.popupMessage}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="${classPrefix}-tooltip-decoration"></div>
                </div>
            `
            const closeDiv = document.getElementsByClassName(`${classPrefix}-tooltip-close`)[0];
            closeDiv.addEventListener("click", function(e) {
                e.stopPropagation();
                const tooltip = document.getElementsByClassName(`${classPrefix}-tooltip-body initial-msg`)[0];
                tooltip.style.display = "none";
                window.sessionStorage.setItem("indicatorShown", true);
            });
    
            const indicator = document.getElementsByClassName("initial-msg")[0];
    
            // Only fade out indicator on mobile devices
            if(window.screen.width <= 425) {
                // Fade out after 5 seconds
                setTimeout(function() {
                    if(indicator.animate) {
                        indicator.animate([
                                // keyframes
                                { opacity: '100%' },
                                { opacity: '0%' },
                            ], {
                            // timing options
                            duration: 500,
                            iterations: 1,
                            // retain after animation is over
                            fill: "forwards"
                        });
                    } else {
                        indicator.style.display = "none";
                    }
                }, 10000);
            }
        });
    }
}

/**
 ****************************************************************
 * WIDGET INITIALIZATION WITH METADATA
 ****************************************************************
 */

async function fetchWithTimeout(resource, options = {}) {
    /* Source: https://dmitripavlutin.com/timeout-fetch-request/ */
    const { timeout = 8000 } = options;

    const controller = new AbortController();
    const id = setTimeout(() => controller.abort(), timeout);
    const response = await fetch(resource, {
        ...options,
        signal: controller.signal
    });
    clearTimeout(id);
    return response;
}


// TODO exception handling
async function makeRequest(url = '', data = {}, extraHeaders = {}, method = 'POST') {
    let options = {
        method: method, // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'default', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'omit', // include, *same-origin, omit
        headers: {
            'Content-Type': 'application/json',
            ...extraHeaders
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        timeout: 2500,
    }
    if(method === 'POST') {
        options.body = JSON.stringify(data)
    }
    // Default options are marked with *
    const response = await fetchWithTimeout(url, options=options);
    return response; // parses JSON response into native JavaScript objects
}


/**
 * Initialize the BotFront widget
 */
function initializeWidget(attrs) {
    WebChat.default.init({
        selector: "#webchat",
        initPayload: initPayload,
        interval: 1000, // 1000 ms between each message
        customData: customData, // arbitrary custom data. Stay minimal as this will be added to the socket
        socketUrl: baseUrl, // chatbot.tilburg.io
        socketPath: socketPath,
        title: attrs.widgetTitle,
        subtitle: attrs.widgetSubtitle,
        inputTextFieldHint: "Typ hier je vraag...",
        connectOn: "open",
        hideWhenNotConnected: false,
        onSocketEvent: onSocketEvent,
        connectingText: "Het lukt niet om verbinding te maken met de chatvoorziening",
        profileAvatar: attrs.avatarUrl,
        showCloseButton: true,
        fullScreenMode: false,
        params: {storage: "session"},
        customMessageDelay: (message) => {
            let delay = message.length * 20;
            if (delay > 2 * 1000) delay = 2000;
            if (delay < 500) delay = 500;
            return delay;
        }
    })

    addWidgetProps();
}


/* Check whether the user has an active session with Gem */
function chatSessionExists() {
    let chatSession = window.sessionStorage.getItem("chat_session");
    return !!JSON.parse(chatSession)?.conversation?.length
}


function initializeGem(attrs) {
    if(!attrs.gemEnabled && !chatSessionExists()) return;

    if(!livechat && ["Dordrecht", "Sliedrecht", "Alblasserdam", "Hendrik-Ido-Ambacht", "Zwijndrecht"].includes(municipality)) {
        // Temporary LiveCom integration for Drechtsteden
    
        waitUntilElementExists(".lc5element.floatingbutton", (el) => {
            // Do not remove our own livecom button
            if(!el.classList.contains("gem-livecom-button")) {
                el.remove()
            }
        }, timeout=10);
    }

    function initCallback() {
        initializeWidget(attrs);
        addWidgetIndicator(attrs);
    }

    if(attrs.directHandover) {
        initPayload = "/direct_escalatie"
        // Guid only exists when OBI script is used
        if(livechat) initializeLivechat(guid, initCallback, attrs)
        else initializeLivechat(initCallback, attrs)
    } else {
        initCallback()
    }

    let openWidget = new URLSearchParams(window.location.search).get("open-widget")
    if(openWidget === "true") {
        WebChat.open()
    }
}

class DomainMetadata {
    storageKey = "domainMetadata";

    getMetadata(websiteDomain) {
        console.log("Retrieving domain metadata from endpoint")

        // Workaround for antwoord CMS
        if(
            websiteDomain.includes("mijn.test.virtuele-gemeente-assistent.nl")
            || websiteDomain.includes("mijn.virtuele-gemeente-assistent.nl")
        ) {
            websiteDomain = `#${municipality}`;
        }

        let gemBaseUrl = domain.split(".").slice(-3).join(".")
        let domainMetadataUrl = `${scriptUrl[0]}//${gemBaseUrl}/domain-metadata-api`;

        domainMetadataUrl = `${domainMetadataUrl}?domain=${encodeURIComponent(websiteDomain)}`
        return makeRequest(domainMetadataUrl, { }, { "Cache-Control": "max-age=300, private" }, "GET")
        .then(response => {
            if(!response || (!!response && response.status == 503)) {
                throw "Antwoorden CMS domain metadata API not available";
            }

            if(!!response && response.status === 200) {
                return response.json()
            }
        })
        .then(metadata => {
            this.data = metadata;
            return metadata
        })
        .catch(function(error) {
            // Antwoorden CMS most likely not available, do not show the widget
            console.log("Error occurred while retrieving domain metadata for Gem, not displaying widget");
            console.log(error)
        });
    }
}


async function initializeWithMetadata(pageUrl, pageTitle, attrs) {
    const domainMetadata = new DomainMetadata()

    domainMetadata.getMetadata(document.domain).then((cachedData) => {
        let path = window.location.pathname.replace(/\/+$/, '');
        if(!path) path = "/"

        // let cachedData = domainMetadata.data;
        if(!cachedData) {
            console.log("No domain metadata found, initializing widget")
            initializeGem(attrs)
            return
        }

        if(path in cachedData.enabledPages) {
            for(const prop in cachedData.defaults) {
                // Only override with empty value if allowed
                if(cachedData.defaults[prop] || allowEmpty[prop]) {
                    attrs[prop] = cachedData.defaults[prop];
                }
            }

            for(const prop in cachedData.enabledPages[path]) {
                // Only override with empty value if allowed
                if(cachedData.enabledPages[path][prop] || allowEmpty[prop]) {
                    attrs[prop] = cachedData.enabledPages[path][prop];
                }
            }

            initializeGem(attrs)
        } else if(!cachedData.disabledPages.includes(path)) {
            let gemBaseUrl = domain.split(".").slice(-3).join(".")
            let pageMetadataUrl = `${scriptUrl[0]}//${gemBaseUrl}/metadata-api`;

            // Workaround for antwoord CMS
            if(
                pageUrl.includes("mijn.test.virtuele-gemeente-assistent.nl")
                || pageUrl.includes("mijn.virtuele-gemeente-assistent.nl")
            ) {
                pageUrl = `#${municipality}`;
            }

            makeRequest(pageMetadataUrl, { url: pageUrl, title: pageTitle })
            .then(response => {
                if(!response || (!!response && response.status == 503)) {
                    throw "Antwoorden CMS metadata API not available";
                }

                if(!response || (!!response && response.status >= 400)) {
                    console.log("Initializing Gem without metadata");
                    return {}
                }
                console.log("Initializing Gem with metadata")
                return response.json()
            }).then(metadata => {
                // Override default attrs with values from metadata API
                for(const prop in metadata) {
                    // Only override with empty value if allowed
                    if(metadata[prop] || allowEmpty[prop]) {
                        attrs[prop] = metadata[prop];
                    }
                }

                initializeGem(attrs)
            })
            .catch(function(error) {
                // Antwoorden CMS most likely not available, do not show the widget
                console.log("Error occurred while retrieving metadata for Gem, not displaying widget");
                console.log(error)
            });
        }
    });
}

initializeWithMetadata(window.location.href, document.title, customAttrs);

// Rerender the widget in case of React making changes to the DOM
function addReactWidgetObserver() {
    let usesReact = !!document.querySelector("[data-reactroot]");

    if (!usesReact) return

    let targetNode = document.querySelector('title');

    if (!targetNode) return

    // Options for the observer (which mutations to observe)
    let config = { attributes: true, childList: true, subtree: true };

    // Callback function to execute when mutations are observed
    let callback = (mutationList, observer) => {
        let title = mutationList[0].addedNodes[0].data
        initializeWithMetadata(window.location.href, title, customAttrs);
    };

    // Create an observer instance linked to the callback function
    const observer = new MutationObserver(callback);

    // Start observing the target node for configured mutations
    observer.observe(targetNode, config);
}

addReactWidgetObserver()
