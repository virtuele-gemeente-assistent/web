// Source: https://stackoverflow.com/a/60298192
function getUrl(currentUrl) {
  if (currentUrl.includes("localhost")) {
    return "localhost"
  }
  const r =  /.*\.([^.]*[^0-9][^.]*\.[^.]*[^.0-9][^.]*$)/;

  var baseDomain = currentUrl.replace(r, "$1");

  // TODO remove hardcoded
  if (currentUrl.includes("test")) {
      baseDomain = "test." + baseDomain
  } else if(currentUrl.includes("develop")) {
      baseDomain = "develop." + baseDomain
  }
  return baseDomain
}

(function() {
    // Use the correct URL for the anchor tags for different municipalities
    const linkDiv = document.getElementById("chatbot-urls");
    if(!linkDiv) return;

    var urls = linkDiv.getElementsByTagName("a");

    const currentUrl = document.domain;
    for(var i = 0; i < urls.length; i++) {
        if(urls[i].id === "landelijk") {
            urls[i].href = location.protocol + "//" + getUrl(currentUrl);
        } else {
            urls[i].href = location.protocol + "//" + urls[i].id + "." + getUrl(currentUrl);
        }
    }
})();

(function () {
    // Reset button
    var spanElement = document.getElementsByTagName("span")[0];
    spanElement.addEventListener('click', function(){if (window.localStorage) {window.localStorage.clear();} if (window.sessionStorage) {window.sessionStorage.clear();}window.location = window.location;}, false);
})();

(function () {
    const skipToChatbot = document.querySelector("#skip-to-chatbot");
    if(skipToChatbot) {
        skipToChatbot.addEventListener("click", (e) => {
            document.querySelector('.rw-launcher').click();
        });
    }
})();