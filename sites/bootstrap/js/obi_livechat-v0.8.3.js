// TODO 
// name for client

let engageChat;
let chat;
let target_municipality;
var classPrefix = "rw";

const url = 'https://cloudstatic.obi4wan.com/chat/obi-launcher.js';
const livechatScript = document.getElementById("livechat-script");
const guid = livechatScript.dataset.guid;
const dataConfig = true;
const chatUserName = 'Gem livechat';
const initialMessageToEngage = 'Chat started';

const avatarUrl = livechatScript.dataset.avatar || "/img/avatar.png";

// TODO import this
function getUrl(currentUrl) {
    if (currentUrl.includes("localhost")) {
        return "localhost"
    }
    const r =  /.*\.([^.]*[^0-9][^.]*\.[^.]*[^.0-9][^.]*$)/;

    var baseDomain = currentUrl.replace(r, "$1");
    if (currentUrl.includes("test")) {
        baseDomain = "test." + baseDomain
    }
    return baseDomain
}

const init = (municipality, history = null) => {
    target_municipality = municipality.toLowerCase();
    loadScript(history);
    initMessageForm();
}

const loadScript = (history) => {
    var script = document.createElement('script');
    script.addEventListener('load', () => engageChat = engageChat || initEngageChat(history));
    script.setAttribute('src', url)
    script.setAttribute('id', 'obi-chat-launcher');
    script.setAttribute('data-guid', guid);
    script.setAttribute('data-config', dataConfig);
    document.head.appendChild(script);
};

// Delay before Rasa exit message and welcome message from OBI
const welcomeMessage = async () => {
    await new Promise(r => setTimeout(r, 1000));
    addInitMessage("Je gaat nu chatten met een medewerker", "rasa-exit");
    await new Promise(r => setTimeout(r, 1000));

    showTypingMessage(true);
    await new Promise(r => setTimeout(r, 2000));
    showTypingMessage(false);
    addMessage({author: {name: "context"}, content: engageChat.config.autoTrigger.text})
}

const initEngageChat = (history) => {
    return OBI.chat({
        headless: true,
        enableLauncher: false,
        onChatInit: () => {
            chat = document.getElementById('messages');
            if (history) {
                var typing = setInterval(function() {
                    if(!document.querySelector(`.${classPrefix}-typing-indication`)) {
                        welcomeMessage();
                        chatInitialized(history);
                        clearInterval(typing);
                    }
                }, 500);
            }
        },
        onNewMessage: (message) => {
            // Both user and agent messages will be send to onNewMessage
            if (message.displayInWidget) addMessage(message);
        },
        onTyping: (isTyping) => {
            showTypingMessage(isTyping.typing);
        }
    });
};

const initMessageForm = () => {
    const form = document.getElementsByClassName(`${classPrefix}-sender`)[0];
    const messageField = document.getElementsByClassName(`${classPrefix}-new-message`)[0];

    form.addEventListener('keypress', (e) => {
        if (e.key === "Enter") {
            e.preventDefault()
            engageChat.sendUserMessage({
                author: {url: null, name: chatUserName},
                content: messageField.value
            });
            form.reset();
        }
    })

    const button = document.getElementsByClassName(`${classPrefix}-send`)[0]
    button.addEventListener('click', (e) => {
        e.preventDefault()
        engageChat.sendUserMessage({
            author: {url: null, name: chatUserName},
            content: messageField.value
        });
        form.reset();
    })
}

const chatInitialized = (history) => {
    content = ""
    for(var i = 0; i < history.length; i++) {
        content += history[i].event + ": " + history[i].text + "\n\n"
    }
    engageChat.sendContextMessage({
        author: {url: null, name: chatUserName},
        content: content,
        displayInWidget: false
    })

    engageChat.sendContextMessage({
        author: {url: null, name: chatUserName},
        displayInWidget: false,
        content: initialMessageToEngage
    });
}

// TODO rename variables
const addMessage = (msg) => {
    var node = document.createElement('div');

    var node2 = document.createElement("div");
    node2.className = `${classPrefix}-message with-avatar`
    node.appendChild(node2)

    var node3 = document.createElement("div");

    if (msg.author.name === chatUserName) {
        node.className = `${classPrefix}-group-message ${classPrefix}-from-client`
        node3.className = `${classPrefix}-client`
    } else {
        node.className = `${classPrefix}-group-message ${classPrefix}-from-response`
        node3.className = `${classPrefix}-response`

        // Add livechat avatar
        var avatar = document.createElement("img")
        avatar.className = `${classPrefix}-avatar`
        avatar.src = avatarUrl;
        node2.appendChild(avatar);
    }
    node2.appendChild(node3)

    var node4 = document.createElement("div");
    node4.className = `${classPrefix}-message-text`
    node3.appendChild(node4)

    node4.innerHTML = msg.content;

    // Store the messages in the session, to display on reload
    var livechatHistory = JSON.parse(window.sessionStorage.getItem("livechat_history")) || [];
    livechatHistory.push(node.outerHTML)
    window.sessionStorage.setItem("livechat_history", JSON.stringify(livechatHistory))

    chat.appendChild(node);

    // Scroll to bottom
    chat.scrollTop = chat.scrollHeight
}

const addInitMessage = (messageText, messageClassPrefix) => {
    var node = document.createElement('div');

    var node2 = document.createElement("div");
    node2.className = `${classPrefix}-message with-avatar context-msg-wrapper`
    node.appendChild(node2)

    var node3 = document.createElement("div");

    node.className = `${classPrefix}-group-message ${classPrefix}-from-response context-msg`
    node3.className = `${classPrefix}-client ${messageClassPrefix}-msg`
    node2.appendChild(node3)

    var node4 = document.createElement("div");
    node4.className = `context-msg-message-text`
    node3.appendChild(node4)

    node4.innerHTML = messageText;

    // Store the messages in the session, to display on reload
    var livechatHistory = JSON.parse(window.sessionStorage.getItem("livechat_history")) || [];
    livechatHistory.push(node.outerHTML)
    window.sessionStorage.setItem("livechat_history", JSON.stringify(livechatHistory));

    chat.appendChild(node);

    // Scroll to bottom
    chat.scrollTop = chat.scrollHeight
}

const showTypingMessage = (typing) => {
    if(typing) {
        var node = document.createElement('div');

        var avatar = document.createElement("img")
        avatar.className = `${classPrefix}-avatar`
        avatar.src = avatarUrl;
        node.appendChild(avatar);

        var node2 = document.createElement("div");
        node2.className = `${classPrefix}-response`
        node.appendChild(node2)

        var node3 = document.createElement("div");

        node.className = `${classPrefix}-message ${classPrefix}-typing-indication ${classPrefix}-with-avatar`

        var node3 = document.createElement("div");
        node3.id = "wave"
        var dot = document.createElement("span");
        dot.className = "rw-dot"

        for(var i=0; i < 3; i++) {
            node3.appendChild(dot.cloneNode(true))
        }

        node2.appendChild(node3)

        chat.appendChild(node);
    } else {
        // Remove typingDivs
        var typingDivs = document.getElementsByClassName(`${classPrefix}-typing-indication`);
        if(typingDivs.length) {
            var div = typingDivs[typingDivs.length - 1];
            div.parentElement.removeChild(div);
        }
    }

    // Scroll to bottom
    chat.scrollTop = chat.scrollHeight
}

// const showTypingMessageCentered = (typing) => {
//     if(typing) {
//         var node = document.createElement("div");
//         node.className = `${classPrefix}-typing-centered`

//         var node3 = document.createElement("div");

//         var node3 = document.createElement("div");
//         node3.id = "wave"
//         var dot = document.createElement("span");
//         dot.className = "rw-dot"

//         for(var i=0; i < 3; i++) {
//             node3.appendChild(dot.cloneNode(true))
//         }

//         node.appendChild(node3)
//         chat.appendChild(node)
//     } else {
//         // Remove typingDivs
//         var typingDivs = document.getElementsByClassName(`${classPrefix}-typing-centered`);
//         if(typingDivs.length) {
//             var div = typingDivs[typingDivs.length - 1];
//             div.parentElement.removeChild(div);
//         }
//     }

//     // Scroll to bottom
//     chat.scrollTop = chat.scrollHeight
// }

const createSystemMessage = (msg) => {
    return {
        content: msg,
        type: 'system',
        date: new Date(),
        author: {
            name: 'system'
        }
    }
}


// Source: https://stackoverflow.com/a/56399194
var waitUntilElementExists = (selector, callback) => {
    const el = document.querySelector(selector);

    if (el) {
        return callback(el);
    }
    
    setTimeout(() => waitUntilElementExists(selector, callback), 500);
}


function livechatHistory(reload=false) {
    const municipality = window.sessionStorage.getItem("municipality");
    if(municipality) {
        // Initialize the livechat integration once the widget form is loaded
        waitUntilElementExists(`.${classPrefix}-sender`, (el) => {
            if(reload) {
                init(municipality); 
            }

            // Display livechat history
            var livechatHistory = JSON.parse(window.sessionStorage.getItem("livechat_history")) || [];

            const messages = document.getElementById("messages");
            for(var i = 0; i < livechatHistory.length; i++) {
                messages.innerHTML += livechatHistory[i];
            }
            messages.scrollTop = messages.scrollHeight
        });
    }
}

// Setup connection if user reloads page
window.addEventListener("load", function() {
    livechatHistory(reload=true);

    // Show livechatHistory after closing/opening widget
    waitUntilElementExists(`.${classPrefix}-launcher`, (el) => {
        const chatButton = document.getElementsByClassName("rw-launcher")[0];

        // On clicking the open button
        chatButton.addEventListener("click", function() {

            // Reload livechat history if the livechat has been started previously
            if(engageChat) {
                livechatHistory(reload=false)
                waitUntilElementExists(`.${classPrefix}-sender`, (el) => {
                    initMessageForm();

                    // Needed to make sure the messages are added to the correct div
                    chat = document.getElementById('messages');
                });
            }
        });
    });
});
