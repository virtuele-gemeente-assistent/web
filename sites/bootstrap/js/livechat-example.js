// Retrieve the custom data
const script = document.getElementById("widget-script");

// Determine socket URL from the src used to include the widget script
const scriptUrl = script.src.split("/");
const baseUrl = scriptUrl[0] + "//" + scriptUrl[2] + "/";
const chatbotAvatarUrl = `${baseUrl}/static/img/avatar.png`;
let socketPath = "/test-livechat/socket.io";

// Retrieve the livechat script
const livechat = document.getElementById("livechat-script");

var classPrefix = "rw";

// Initialize the Rasa widget without livechat
WebChat.default.init({
    selector: "#webchat",
    // initPayload: "",
    interval: 1000, // 1000 ms between each message
    customData: {}, // arbitrary custom data. Stay minimal as this will be added to the socket
    socketUrl: baseUrl, // chatbot.tilburg.io
    socketPath: socketPath,
    title: "Gem",
    subtitle: "Je digitale hulp van de gemeente",
    inputTextFieldHint: "Typ hier je vraag...",
    // onSocketEvent: onSocketEvent,
    connectingText: "Even geduld",
    profileAvatar: chatbotAvatarUrl,
    showCloseButton: true,
    fullScreenMode: false,
    params: {storage: "session"},
    customMessageDelay: (message) => {
        let delay = message.length * 20;
        if (delay > 2 * 1000) delay = 2000;
        if (delay < 500) delay = 500;
        return delay;
    }
})
