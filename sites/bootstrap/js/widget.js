let currentScript = document.querySelector("#chatwidget-script");
const currentScriptUrl = currentScript.src.split("/");
const baseDomain = currentScriptUrl[2];
const baseGemUrl = `${currentScriptUrl[0]}//${baseDomain}`;
const cmsBaseUrl = `${currentScriptUrl[0]}//mijn.${baseDomain}`;

function addScript(src, attrs={}) {
    return new Promise((resolve, reject) => {
        const s = document.createElement('script');

        // Scripts should load synchronously, because there are dependencies
        s.async = false;

        s.setAttribute('src', src);
        for(let property in attrs) {
            s.setAttribute(property, attrs[property]);
        }

        s.addEventListener('load', resolve);
        s.addEventListener('error', reject);

        document.body.appendChild(s);
    });
}

let fetchStyle = function(url) {
    return new Promise((resolve, reject) => {
        let link = document.createElement('link');
        link.type = 'text/css';
        link.rel = 'stylesheet';
        link.onload = () => resolve();
        link.onerror = () => reject();
        link.href = url;

        document.head.appendChild(link);
    });
};


// Defaults for custom attributes
let customWidgetAttrs = {
    gemEnabled: true,
    directHandover: false,
    widgetTitle: "Gem",
    widgetSubtitle: "Je digitale hulp van de gemeente",
    popupMessage: "Stel je vraag",
    avatarUrl: `${baseGemUrl}/static/img/avatar.png`,
    organisationName: null,
    organisationSlug: null,
    livechatType: null,
    livechatId: null,
    livechatAvatarUrl: null,
}

let allowEmptyAttrs = {
    gemEnabled: true,
    directHandover: true,
    widgetTitle: true,
    widgetSubtitle: true,
    popupMessage: true,
    avatarUrl: false,
    organisationName: true,
    organisationSlug: true,
    livechatType: true,
    livechatId: true,
    livechatAvatarUrl: false,
}

// Override default attrs with values from widget attributes
for(const prop in currentScript.dataset) {
    if (!customWidgetAttrs.hasOwnProperty(prop)) continue;

    let overrideValue = script.dataset[prop];
    if (typeof(customWidgetAttrs[prop]) === "boolean") {
        if(overrideValue === "true") overrideValue = true;
        else overrideValue = false;
    }
    customWidgetAttrs[prop] = overrideValue;
}

async function fetchWithTimeout(resource, options = {}) {
    /* Source: https://dmitripavlutin.com/timeout-fetch-request/ */
    const { timeout = 8000 } = options;

    const controller = new AbortController();
    const id = setTimeout(() => controller.abort(), timeout);
    const response = await fetch(resource, {
        ...options,
        signal: controller.signal
    });
    clearTimeout(id);
    return response;
}

// TODO exception handling
async function makeRequest(url = '', data = {}, extraHeaders = {}, method = 'POST') {
    let options = {
        method: method, // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'default', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'omit', // include, *same-origin, omit
        headers: {
            'Content-Type': 'application/json',
            ...extraHeaders
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        timeout: 2500,
    }
    if(method === 'POST') {
        options.body = JSON.stringify(data)
    }
    // Default options are marked with *
    const response = await fetchWithTimeout(url, options=options);
    return response; // parses JSON response into native JavaScript objects
}

function injectScripts(attrs) {
    // Widgetscript attrs
    let widgetAttrs = {
        id: "widget-script",
        "data-gem-enabled": attrs.gemEnabled,
        "data-municipality": attrs.organisationName,
        "data-organisation-slug": attrs.organisationSlug,
        // "data-avatar-url": attrs.avatarUrl,
        "data-direct-handover": attrs.directHandover,
        "data-widget-title": attrs.widgetTitle,
        "data-widget-subtitle": attrs.widgetSubtitle,
        "data-popup-message": attrs.popupMessage,
    }
    // Custom attr, not part of metadata API
    if(currentScript.dataset.clientName) widgetAttrs["data-client-name"] = currentScript.dataset.clientName
    if(attrs.avatarUrl) widgetAttrs["data-avatar-url"] = attrs.avatarUrl

    let baseStyleUrl = `${baseGemUrl}/static/css/widget-v0.11.7.css`;
    let specificStyleUrl = `${cmsBaseUrl}/${attrs.organisationSlug}/_styling`;

    let webchatScriptUrl = `${baseGemUrl}/static/js/webchat-v0.11.7.js`;
    let obiScriptUrl = `${baseGemUrl}/static/js/obi_livechat-v0.11.7.js`;
    let widgetScriptUrl = `${baseGemUrl}/static/js/widget-v0.11.7.js`;

    if(attrs.gemEnabled) {
        fetchStyle(baseStyleUrl);
        fetchStyle(specificStyleUrl);
        addScript(webchatScriptUrl);
        // if(attrs.livechatType === "obi4wan") {
        //     addScript(obiScriptUrl, attrs={
        //         id: "livechat-script",
        //         "data-guid": attrs.livechatId,
        //         "data-name": attrs.livechatType,
        //         "data-avatar": attrs.livechatAvatarUrl,
        //     });
        // }
        addScript(widgetScriptUrl, attrs=widgetAttrs);
    }
}


class CustomDomainMetadata {
    storageKey = "domainMetadata";

    getMetadata(websiteDomain) {
        console.log("Retrieving domain metadata from endpoint")

        // Workaround for antwoord CMS
        if(
            websiteDomain.includes("mijn.test.virtuele-gemeente-assistent.nl")
            || websiteDomain.includes("mijn.virtuele-gemeente-assistent.nl")
        ) {
            let municipalityName = JSON.parse((document.querySelector("#organisation-name") || {}).textContent || "\"\"");
            websiteDomain = `#${municipalityName}`;
        }

        let gemBaseUrl = baseDomain.split(".").slice(-3).join(".")
        let domainMetadataUrl = `${currentScriptUrl[0]}//${gemBaseUrl}/domain-metadata-api`;

        domainMetadataUrl = `${domainMetadataUrl}?domain=${encodeURIComponent(websiteDomain)}`
        return makeRequest(domainMetadataUrl, { }, { "Cache-Control": "max-age=300, private" }, "GET")
        .then(response => {
            if(!response || (!!response && response.status == 503)) {
                throw "Antwoorden CMS domain metadata API not available";
            }

            if(!!response && response.status === 200) {
                return response.json()
            }
        })
        .then(metadata => {
            this.data = metadata;
            return metadata
        })
        .catch(function(error) {
            // Antwoorden CMS most likely not available, do not show the widget
            console.log("Error occurred while retrieving domain metadata for Gem, not displaying widget");
            console.log(error)
        });
    }
}


async function initializeWithMetadata(pageUrl, pageTitle, attrs) {
    const domainMetadata = new CustomDomainMetadata()

    domainMetadata.getMetadata(document.domain).then((cachedData) => {
        let path = window.location.pathname.replace(/\/+$/, '');
        if(!path) path = "/"

        // let cachedData = domainMetadata.data;
        if(!cachedData) {
            console.log("No domain metadata found, initializing widget")
            injectScripts({})
            return
        }

        if(path in cachedData.enabledPages) {
            for(const prop in cachedData.defaults) {
                // Only override with empty value if allowed
                if(cachedData.defaults[prop] || allowEmptyAttrs[prop]) {
                    attrs[prop] = cachedData.defaults[prop];
                }
            }

            for(const prop in cachedData.enabledPages[path]) {
                // Only override with empty value if allowed
                if(cachedData.enabledPages[path][prop] || allowEmptyAttrs[prop]) {
                    attrs[prop] = cachedData.enabledPages[path][prop];
                }
            }

            injectScripts(attrs);
        } else if(!cachedData.disabledPages.includes(path)) {
            let gemBaseUrl = baseDomain.split(".").slice(-3).join(".")
            let pageMetadataUrl = `${currentScriptUrl[0]}//${gemBaseUrl}/metadata-api`;

            // Workaround for antwoord CMS
            if(
                pageUrl.includes("mijn.test.virtuele-gemeente-assistent.nl")
                || pageUrl.includes("mijn.virtuele-gemeente-assistent.nl")
            ) {
                let municipalityName = JSON.parse((document.querySelector("#organisation-name") || {}).textContent || "\"\"");
                pageUrl = `#${municipalityName}`;
            }

            makeRequest(pageMetadataUrl, { url: pageUrl, title: pageTitle })
            .then(response => {
                if(!response || (!!response && response.status == 503)) {
                    throw "Antwoorden CMS metadata API not available";
                }

                if(!response || (!!response && response.status >= 400)) {
                    console.log("Initializing Gem without metadata");
                    return {}
                }
                console.log("Initializing Gem with metadata")
                return response.json()
            }).then(metadata => {
                // Override default attrs with values from metadata API
                for(const prop in metadata) {
                    // Only override with empty value if allowed
                    if(metadata[prop] || allowEmptyAttrs[prop]) {
                        attrs[prop] = metadata[prop];
                    }
                }

                injectScripts(metadata);
            })
            .catch(function(error) {
                // Antwoorden CMS most likely not available, do not show the widget
                console.log("Error occurred while retrieving metadata for Gem, not displaying widget");
                console.log(error)
            });
        }
    });
}

initializeWithMetadata(window.location.href, document.title, customWidgetAttrs);
