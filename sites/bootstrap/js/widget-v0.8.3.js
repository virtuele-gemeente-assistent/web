// Retrieve the custom data
const script = document.getElementById("widget-script");
const municipality = script.dataset.municipality;
const nlxOrganizationName = script.dataset.nlxOrgName;
const nlxDemo = script.dataset.nlxDemo;

// Determine socket URL from the src used to include the widget script
const scriptUrl = script.src.split("/");
const baseUrl = scriptUrl[0] + "//" + scriptUrl[2] + "/";
const chatbotAvatarUrl = `${baseUrl}/static/img/avatar.png`;

// Retrieve the livechat script
const livechat = document.getElementById("livechat-script");

var classPrefix = "rw";

var initPayload = "/greet";
if(municipality) {
    initPayload = `/greet{\"municipality\":\"${municipality}\"}`
}

var customData = {};
if(municipality) {
    customData.municipality = municipality;

    if(nlxDemo) {
        customData.nlx_demo = true;
    }

    if(nlxOrganizationName) {
        customData.nlx_org_name = nlxOrganizationName;
    }

}

let onSocketEvent;

if(livechat) {
    switch(livechat.dataset.name) {
        // Currently only supports obi4wan
        case "obi4wan":
            customData.livechat = livechat.dataset.name;
            customData.guid = livechat.dataset.guid;
            onSocketEvent = {
                'bot_uttered': (data) => { if(data.history) {
                    // Store the municipality in the session, needed to restore
                    // the connection with Obi4Wan on page refresh
                    window.sessionStorage.setItem("municipality", data.municipality);

                    init(data.municipality, history=data.history);
                }},
            };
    }
}

// Set the username as custom data for Rasa
const queryString = window.location.search;
if(queryString) {
    const urlParams = new URLSearchParams(queryString);
    const username = urlParams.get("username");
    if(username) customData.username = username;
}

// Initialize the Rasa widget without livechat
WebChat.default.init({
    selector: "#webchat",
    initPayload: initPayload,
    interval: 1000, // 1000 ms between each message
    customData: customData, // arbitrary custom data. Stay minimal as this will be added to the socket
    socketUrl: baseUrl, // chatbot.tilburg.io
    title: "Gem",
    subtitle: "Je digitale hulp van de gemeente",
    inputTextFieldHint: "Typ hier je vraag...",
    onSocketEvent: onSocketEvent,
    connectingText: "Even geduld",
    profileAvatar: chatbotAvatarUrl,
    showCloseButton: true,
    fullScreenMode: false,
    params: {storage: "session"},
    customMessageDelay: (message) => {
        let delay = message.length * 20;
        if (delay > 2 * 1000) delay = 2000;
        if (delay < 500) delay = 500;
        return delay;
    }
})

// Source: https://stackoverflow.com/a/56399194
var waitUntilElementExists = (selector, callback) => {
    const el = document.querySelector(selector);
    if (el) {
        return callback(el);
    }
    
    setTimeout(() => waitUntilElementExists(selector, callback), 500);
}

function sendUserMessageListener(linkElement, message) {
    linkElement.addEventListener("click", function() {
        const textBar = document.getElementsByClassName(`${classPrefix}-new-message`)[0];
        textBar.value = message;
        document.getElementsByClassName(`${classPrefix}-send`)[0].click()
    });
}

function addMenu() {
    const header = document.getElementsByClassName(`${classPrefix}-with-subtitle`)[0];

    const menu = document.createElement("div");
    menu.className = "dropdown show options-menu";

    // Fade in menu
    menu.animate([
            // keyframes
            { opacity: '0%' }, 
            { opacity: '100%' },
        ], { 
        // timing options
        duration: 500,
        iterations: 1
    });
    
    const button = document.createElement("a");
    button.dataset.toggle = "dropdown";
    button.id = "menuButton";
    button.href = "#"
    button.setAttribute("role", "button");
    button.setAttribute("aria-haspopup", "true")
    button.setAttribute("aria-expanded", "false")
    button.innerHTML = "<div class=\"menu-bar\"></div><div class=\"menu-bar\"></div><div class=\"menu-bar\"></div>";

    menu.appendChild(button);

    // const messagesScreen = document.getElementById("messages");
    const dropdown = document.createElement("div");

    dropdown.className = "dropdown-menu";
    dropdown.setAttribute("aria-labelledby", "menuButton");

    const feedbackLink = document.createElement("a");
    feedbackLink.className = "dropdown-item";
    feedbackLink.href = "#";
    feedbackLink.textContent = "Je mening geven";
    sendUserMessageListener(feedbackLink, "feedback")

    const downloadConversationLink = document.createElement("a");
    downloadConversationLink.className = "dropdown-item";
    downloadConversationLink.href = "#";
    downloadConversationLink.textContent = "Download gesprek";
    sendUserMessageListener(downloadConversationLink, "download gesprek")

    const privacyLink = document.createElement("a");
    privacyLink.className = "dropdown-item";
    privacyLink.href = "#";
    privacyLink.textContent = "Privacy";
    sendUserMessageListener(privacyLink, "privacy")

    dropdown.appendChild(feedbackLink);
    dropdown.appendChild(downloadConversationLink);
    dropdown.appendChild(privacyLink);
    menu.appendChild(dropdown);
    header.appendChild(menu);

    button.addEventListener("click", function(e) {
        e.stopPropagation()
        if(dropdown.classList.contains("show")) {
            dropdown.classList.remove("show")
        } else {
            dropdown.classList.add("show")
        }
    });

}

window.addEventListener("load", function() {
    waitUntilElementExists(`.${classPrefix}-launcher`, (el) => {
        const chatButton = document.getElementsByClassName(`${classPrefix}-launcher`)[0];

        waitUntilElementExists(`.${classPrefix}-with-subtitle`, (el) => {
            addMenu()
        });

        chatButton.addEventListener("click", function() {
            waitUntilElementExists(`.${classPrefix}-with-subtitle`, (el) => {
                addMenu()
            });
        });
    });
});

if(!window.sessionStorage.getItem("indicatorShown")) {
    // Add a tooltip for a new session
    waitUntilElementExists(`.${classPrefix}-open-launcher__container`, (el) => {
        var launcher = document.getElementsByClassName(`${classPrefix}-open-launcher__container`)[0];
        launcher.innerHTML += `
            <div class="${classPrefix}-tooltip-body initial-msg">
                <div class="${classPrefix}-tooltip-close">
                    <button>
                        <img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjUxMnB4IiBoZWlnaHQ9IjUxMnB4IiB2aWV3Qm94PSIwIDAgMzU3IDM1NyIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMzU3IDM1NzsiIHhtbDpzcGFjZT0icHJlc2VydmUiPgo8Zz4KCTxnIGlkPSJjbGVhci1ncmV5Ij4KCQk8cG9seWdvbiBwb2ludHM9IjM1NywzNS43IDMyMS4zLDAgMTc4LjUsMTQyLjggMzUuNywwIDAsMzUuNyAxNDIuOCwxNzguNSAwLDMyMS4zIDM1LjcsMzU3IDE3OC41LDIxNC4yIDMyMS4zLDM1NyAzNTcsMzIxLjMgICAgIDIxNC4yLDE3OC41ICAgIiBmaWxsPSIjYWFhYWFhIi8+Cgk8L2c+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==" alt="close">
                    </button></div>
                    <div class="${classPrefix}-tooltip-response initial-msg">
                        <div class="${classPrefix}-response" style="">
                            <div class="${classPrefix}-message-text"><div class="${classPrefix}-markdown">
                                <p>Chat met Gem!</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="${classPrefix}-tooltip-decoration"></div>
            </div>
        `
        const closeDiv = document.getElementsByClassName(`${classPrefix}-tooltip-close`)[0];
        closeDiv.addEventListener("click", function(e) {
            e.stopPropagation();
            const tooltip = document.getElementsByClassName(`${classPrefix}-tooltip-body initial-msg`)[0];
            tooltip.style.display = "none";
            window.sessionStorage.setItem("indicatorShown", true);
        });

        const indicator = document.getElementsByClassName("initial-msg")[0];

        // Fade out after 5 seconds
        setTimeout(function() {
            indicator.animate([
                    // keyframes
                    { opacity: '100%' }, 
                    { opacity: '0%' },
                ], { 
                // timing options
                duration: 500,
                iterations: 1,
                // retain after animation is over
                fill: "forwards"
            });
        }, 5000);
    });
}
