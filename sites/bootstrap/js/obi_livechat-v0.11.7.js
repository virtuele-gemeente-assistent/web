let engageChat;
let chat;
let target_municipality;
let formEventBound = false;
var widgetClassPrefix = "rw";
var typingTimeout;
var isTyping = false;

const url = "https://cloudstatic.obi4wan.com/chat/obi-launcher.js";
const livechatScript = document.getElementById("livechat-script");
const guid =
  window.sessionStorage.getItem("activeObiSession") ||
  livechatScript.dataset.guid;
const dataConfig = true;
let chatUserName;
const initialMessageToEngage = "Chat started";

const avatarUrl = livechatScript.dataset.avatar || "/img/avatar.png";

// TODO import this
function getUrl(currentUrl) {
  if (currentUrl.includes("localhost")) {
    return "localhost";
  }
  const r = /.*\.([^.]*[^0-9][^.]*\.[^.]*[^.0-9][^.]*$)/;

  var baseDomain = currentUrl.replace(r, "$1");
  if (currentUrl.includes("test")) {
    baseDomain = "test." + baseDomain;
  }
  return baseDomain;
}

const init = (municipality, history = null) => {
  target_municipality = municipality.toLowerCase();
  chatUserName = `Gem livechat ${municipality}`;
  loadScript(history);
  waitUntilElementExists(`.${widgetClassPrefix}-sender`, (el) => {
    livechatHistory(false);
    initMessageForm();

    // Store the active GUID in the session, to ensure that this same GUID is used, even
    // on pages with different GUIDs configured
    window.sessionStorage.setItem("activeObiSession", guid);
  });
};

const loadScript = (history) => {
  var script = document.createElement("script");
  script.addEventListener(
    "load",
    () => (engageChat = engageChat || initEngageChat(history))
  );
  script.setAttribute("src", url);
  script.setAttribute("id", "obi-chat-launcher");
  script.setAttribute("data-guid", guid);
  script.setAttribute("data-config", dataConfig);
  document.head.appendChild(script);
};

// Delay before Rasa exit message and welcome message from OBI
const welcomeMessage = async () => {
  var livechatHistory =
    JSON.parse(window.sessionStorage.getItem("livechat_history")) || [];
  if (livechatHistory.length > 0) return;
  await new Promise((r) => setTimeout(r, 1000));
  addInitMessage("Je gaat nu chatten met een medewerker", "rasa-exit");
  await new Promise((r) => setTimeout(r, 1000));

  // Show typing indicators and display welcome message, if defined
  let obiWelcomeMessage = engageChat.config.autoTrigger.text;
  if (obiWelcomeMessage) {
    showTypingMessage(true);
    await new Promise((r) => setTimeout(r, 2000));
    showTypingMessage(false);

    addMessage({ author: { name: "context" }, content: obiWelcomeMessage });
  }
};

const initEngageChat = (history) => {
  return OBI.chat({
    headless: true,
    enableLauncher: false,
    onChatInit: () => {
      chat = document.getElementById(`${widgetClassPrefix}-messages`);
      if (history) {
        var typing = setInterval(function () {
          if (
            !document.querySelector(`.${widgetClassPrefix}-typing-indication`)
          ) {
            welcomeMessage();
            waitUntilElementExists(`.${widgetClassPrefix}-sender`, (el) => {
              initMessageForm();
              livechatHistory(false);
            });

            // Only send if not sent before
            if (!window.sessionStorage.getItem("obiChatInitialized")) {
              chatInitialized(history);
            }
            window.sessionStorage.setItem("obiChatInitialized", true);
            clearInterval(typing);
          }
        }, 500);
      }
    },
    onNewMessage: (message) => {
      // Both user and agent messages will be send to onNewMessage
      if (message.displayInWidget) addMessage(message);
    },
    onTyping: (isTyping) => {
      showTypingMessage(isTyping.typing);
    },
  });
};

const initMessageForm = () => {
  formEventBound = true;
  const form = document.getElementsByClassName(
    `${widgetClassPrefix}-sender`
  )[0];
  const livechatBound = form.dataset.livechatBound;
  if (livechatBound) {
    return;
  }
  const messageField = document.getElementsByClassName(
    `${widgetClassPrefix}-new-message`
  )[0];
  function obiMessageHandler(event) {
    event.preventDefault(); // Prevent adding a newline in the input field
    event.stopImmediatePropagation(); // Prevent sending the message to Rasa
    const value = messageField.value;
    
    if (!value || value.length === 0) return;

    engageChat.sendUserMessage({
      author: { url: null, name: chatUserName },
      content: value,
    });
    messageField.value = ""; // Clear the input field
  }

  if (form) {
    form.dataset.livechatBound = true;
    form.addEventListener("keyup", (e) => {
      if (e.key === "Enter") {
        return;
      }
      typingTimeout && clearTimeout(typingTimeout);
      typingTimeout = setTimeout(() => {
        engageChat.setIsTyping(false);
        isTyping = false;
      }, 1000);
    });
    form.addEventListener(
      "keydown",
      (e) => {
        if (e.key === "Enter") {
          typingTimeout && clearTimeout(typingTimeout);
          engageChat.setIsTyping(false);
          isTyping = false;
          obiMessageHandler(e);
        } else {
          if (!isTyping) {
            isTyping = true;
            engageChat.setIsTyping(true);
          }
        }
      },
      true
    );
  }

  const button = document.getElementsByClassName(
    `${widgetClassPrefix}-send`
  )[0];

  if (button) {
    button.disabled = false;
    button.addEventListener(
      "click",
      (e) => {
        obiMessageHandler(e);
      },
      true
    );
  }
};

const chatInitialized = (history) => {
  content = "";
  for (var i = 0; i < history.length; i++) {
    content += history[i].event + ": " + history[i].text + "\n\n";
  }
  engageChat.sendContextMessage({
    author: { url: null, name: chatUserName },
    content: content,
    displayInWidget: false,
  });

  engageChat.sendContextMessage({
    author: { url: null, name: chatUserName },
    displayInWidget: false,
    content: initialMessageToEngage,
  });
};

// TODO rename variables
const addMessage = (msg) => {
  var node = document.createElement("div");
  var node2 = document.createElement("div");
  node2.className = `${widgetClassPrefix}-message with-avatar`;
  node.appendChild(node2);

  var node3 = document.createElement("div");

  let ariaLabel;

  if (msg.author.name === chatUserName) {
    node.className = `${widgetClassPrefix}-group-message ${widgetClassPrefix}-from-client`;
    node3.className = `${widgetClassPrefix}-client`;

    ariaLabel = `U zegt:`;
  } else {
    node.className = `${widgetClassPrefix}-group-message ${widgetClassPrefix}-from-response`;
    node3.className = `${widgetClassPrefix}-response`;

    ariaLabel = `Medewerker ${municipality} zegt:`;

    // Add livechat avatar
    var avatar = document.createElement("img");
    avatar.className = `${widgetClassPrefix}-avatar`;
    avatar.alt = `Logo van gemeente ${municipality}`;
    avatar.src = avatarUrl;
    node2.appendChild(avatar);
  }
  node2.appendChild(node3);

  var node4 = document.createElement("div");
  node4.className = `${widgetClassPrefix}-message-text`;
  node4.setAttribute("aria-label", ariaLabel);
  node3.appendChild(node4);

  node4.innerHTML = msg.content;

  // Store the messages in the session, to display on reload
  var livechatHistory =
    JSON.parse(window.sessionStorage.getItem("livechat_history")) || [];
  node.dataset.livechatIndex = livechatHistory.length + 1;
  livechatHistory.push(node.outerHTML);
  window.sessionStorage.setItem(
    "livechat_history",
    JSON.stringify(livechatHistory)
  );

  chat = document.getElementById(`${widgetClassPrefix}-messages`);

  // Do not attempt to add if widget closed
  if (!chat) return;

  chat.appendChild(node);

  // Scroll to bottom
  chat.scrollTop = chat.scrollHeight;
};

const addInitMessage = (messageText, messageClassPrefix) => {
  var node = document.createElement("div");

  var node2 = document.createElement("div");
  node2.className = `${widgetClassPrefix}-message with-avatar context-msg-wrapper`;
  node.appendChild(node2);

  var node3 = document.createElement("div");

  node.className = `${widgetClassPrefix}-group-message ${widgetClassPrefix}-from-response context-msg`;
  node3.className = `${widgetClassPrefix}-client ${messageClassPrefix}-msg`;
  node2.appendChild(node3);

  var node4 = document.createElement("div");
  node4.className = `context-msg-message-text`;
  node4.setAttribute("aria-label", "Systeembericht: ");
  node4.setAttribute("role", "text");
  node3.appendChild(node4);

  node4.innerHTML = messageText;

  // Store the messages in the session, to display on reload
  var livechatHistory =
    JSON.parse(window.sessionStorage.getItem("livechat_history")) || [];
  node.dataset.livechatIndex = livechatHistory.length + 1;
  livechatHistory.push(node.outerHTML);
  window.sessionStorage.setItem(
    "livechat_history",
    JSON.stringify(livechatHistory)
  );

  chat = document.getElementById(`${widgetClassPrefix}-messages`);

  // Do not attempt to add if widget closed
  if (!chat) return;

  chat.appendChild(node);

  // Scroll to bottom
  chat.scrollTop = chat.scrollHeight;
};

const showTypingMessage = (typing) => {
  chat = document.getElementById(`${widgetClassPrefix}-messages`);
  if (!chat) return;

  if (typing) {
    var node = document.createElement("div");

    var avatar = document.createElement("img");
    avatar.className = `${widgetClassPrefix}-avatar`;
    avatar.alt = `Logo van gemeente ${municipality}`;
    avatar.src = avatarUrl;
    node.appendChild(avatar);

    var node2 = document.createElement("div");
    node2.className = `${widgetClassPrefix}-response`;
    node.appendChild(node2);

    var node3 = document.createElement("div");

    node.className = `${widgetClassPrefix}-message ${widgetClassPrefix}-typing-indication ${widgetClassPrefix}-with-avatar`;

    var node3 = document.createElement("div");
    node3.id = "wave";
    var dot = document.createElement("span");
    dot.className = "rw-dot";

    for (var i = 0; i < 3; i++) {
      node3.appendChild(dot.cloneNode(true));
    }

    node2.appendChild(node3);

    chat.appendChild(node);
  } else {
    // Remove typingDivs
    var typingDivs = document.getElementsByClassName(
      `${widgetClassPrefix}-typing-indication`
    );
    if (typingDivs.length) {
      var div = typingDivs[typingDivs.length - 1];
      div.parentElement.removeChild(div);
    }
  }

  // Scroll to bottom
  chat.scrollTop = chat.scrollHeight;
};

// const showTypingMessageCentered = (typing) => {
//     if(typing) {
//         var node = document.createElement("div");
//         node.className = `${widgetClassPrefix}-typing-centered`

//         var node3 = document.createElement("div");

//         var node3 = document.createElement("div");
//         node3.id = "wave"
//         var dot = document.createElement("span");
//         dot.className = "rw-dot"

//         for(var i=0; i < 3; i++) {
//             node3.appendChild(dot.cloneNode(true))
//         }

//         node.appendChild(node3)
//         chat.appendChild(node)
//     } else {
//         // Remove typingDivs
//         var typingDivs = document.getElementsByClassName(`${widgetClassPrefix}-typing-centered`);
//         if(typingDivs.length) {
//             var div = typingDivs[typingDivs.length - 1];
//             div.parentElement.removeChild(div);
//         }
//     }

//     // Scroll to bottom
//     chat.scrollTop = chat.scrollHeight
// }

const createSystemMessage = (msg) => {
  return {
    content: msg,
    type: "system",
    date: new Date(),
    author: {
      name: "system",
    },
  };
};

// Source: https://stackoverflow.com/a/56399194
var waitUntilElementExists = (selector, callback) => {
  const el = document.querySelector(selector);

  if (el) {
    return callback(el);
  }

  setTimeout(() => waitUntilElementExists(selector, callback), 500);
};

function livechatHistory(reload = false) {
  const municipality = window.sessionStorage.getItem("municipality");
  if (municipality) {
    // Initialize the livechat integration once the widget form is loaded
    waitUntilElementExists(`.${widgetClassPrefix}-sender`, (el) => {
      if (reload) {
        init(municipality);
      }

      // Display livechat history
      var livechatHistory =
        JSON.parse(window.sessionStorage.getItem("livechat_history")) || [];

      const messages = document.getElementById(`${widgetClassPrefix}-messages`);
      const lastExistingNode =
        messages.childNodes[messages.childNodes.length - 1];
      const lastLivechatNodeIndex = lastExistingNode.dataset.livechatIndex || 0;

      for (var i = lastLivechatNodeIndex; i < livechatHistory.length; i++) {
        messages.innerHTML += livechatHistory[i];
      }
      messages.scrollTop = messages.scrollHeight;
    });
  }
}

// Setup connection if user reloads page
livechatHistory((reload = true));

// Show livechatHistory after closing/opening widget
waitUntilElementExists(`.${widgetClassPrefix}-launcher`, (el) => {
  const chatButton = document.getElementsByClassName("rw-launcher")[0];

  // On clicking the open button
  chatButton.addEventListener("click", function () {
    // Reload livechat history if the livechat has been started previously
    if (engageChat) {
      livechatHistory((reload = false));
      waitUntilElementExists(`.${widgetClassPrefix}-sender`, (el) => {
        initMessageForm();

        // Needed to make sure the messages are added to the correct div
        chat = document.getElementById(`${widgetClassPrefix}-messages`);
      });
    }
  });
});
