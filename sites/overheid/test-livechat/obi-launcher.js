var OBI = function(t) {
    var e = {};

    function n(r) {
        if (e[r]) return e[r].exports;
        var o = e[r] = {
            i: r,
            l: !1,
            exports: {}
        };
        return t[r].call(o.exports, o, o.exports, n), o.l = !0, o.exports
    }
    return n.m = t, n.c = e, n.d = function(t, e, r) {
        n.o(t, e) || Object.defineProperty(t, e, {
            enumerable: !0,
            get: r
        })
    }, n.r = function(t) {
        "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {
            value: "Module"
        }), Object.defineProperty(t, "__esModule", {
            value: !0
        })
    }, n.t = function(t, e) {
        if (1 & e && (t = n(t)), 8 & e) return t;
        if (4 & e && "object" == typeof t && t && t.__esModule) return t;
        var r = Object.create(null);
        if (n.r(r), Object.defineProperty(r, "default", {
                enumerable: !0,
                value: t
            }), 2 & e && "string" != typeof t)
            for (var o in t) n.d(r, o, function(e) {
                return t[e]
            }.bind(null, o));
        return r
    }, n.n = function(t) {
        var e = t && t.__esModule ? function() {
            return t.default
        } : function() {
            return t
        };
        return n.d(e, "a", e), e
    }, n.o = function(t, e) {
        return Object.prototype.hasOwnProperty.call(t, e)
    }, n.p = "", n(n.s = 6)
}([function(t, e, n) {
    (function(e, r) {
        /*!
         * @overview es6-promise - a tiny implementation of Promises/A+.
         * @copyright Copyright (c) 2014 Yehuda Katz, Tom Dale, Stefan Penner and contributors (Conversion to ES6 API by Jake Archibald)
         * @license   Licensed under MIT license
         *            See https://raw.githubusercontent.com/stefanpenner/es6-promise/master/LICENSE
         * @version   3.3.1
         */
        ! function(e, n) {
            t.exports = n()
        }(0, function() {
            "use strict";

            function t(t) {
                return "function" == typeof t
            }
            var o = Array.isArray ? Array.isArray : function(t) {
                    return "[object Array]" === Object.prototype.toString.call(t)
                },
                i = 0,
                a = void 0,
                s = void 0,
                u = function(t, e) {
                    y[i] = t, y[i + 1] = e, 2 === (i += 2) && (s ? s(g) : v())
                };
            var c = "undefined" != typeof window ? window : void 0,
                l = c || {},
                f = l.MutationObserver || l.WebKitMutationObserver,
                h = "undefined" == typeof self && void 0 !== e && "[object process]" === {}.toString.call(e),
                d = "undefined" != typeof Uint8ClampedArray && "undefined" != typeof importScripts && "undefined" != typeof MessageChannel;

            function p() {
                var t = setTimeout;
                return function() {
                    return t(g, 1)
                }
            }
            var y = new Array(1e3);

            function g() {
                for (var t = 0; t < i; t += 2) {
                    (0, y[t])(y[t + 1]), y[t] = void 0, y[t + 1] = void 0
                }
                i = 0
            }
            var v = void 0;

            function b(t, e) {
                var n = arguments,
                    r = this,
                    o = new this.constructor(_);
                void 0 === o[w] && k(o);
                var i = r._state;
                return i ? function() {
                    var t = n[i - 1];
                    u(function() {
                        return R(i, o, t, r._result)
                    })
                }() : j(r, o, t, e), o
            }

            function m(t) {
                if (t && "object" == typeof t && t.constructor === this) return t;
                var e = new this(_);
                return A(e, t), e
            }
            v = h ? function() {
                return e.nextTick(g)
            } : f ? function() {
                var t = 0,
                    e = new f(g),
                    n = document.createTextNode("");
                return e.observe(n, {
                        characterData: !0
                    }),
                    function() {
                        n.data = t = ++t % 2
                    }
            }() : d ? function() {
                var t = new MessageChannel;
                return t.port1.onmessage = g,
                    function() {
                        return t.port2.postMessage(0)
                    }
            }() : void 0 === c ? function() {
                try {
                    var t = n(10);
                    return a = t.runOnLoop || t.runOnContext,
                        function() {
                            a(g)
                        }
                } catch (t) {
                    return p()
                }
            }() : p();
            var w = Math.random().toString(36).substring(16);

            function _() {}
            var T = void 0,
                x = 1,
                E = 2,
                C = new U;

            function O(t) {
                try {
                    return t.then
                } catch (t) {
                    return C.error = t, C
                }
            }

            function S(e, n, r) {
                n.constructor === e.constructor && r === b && n.constructor.resolve === m ? function(t, e) {
                    e._state === x ? B(t, e._result) : e._state === E ? I(t, e._result) : j(e, void 0, function(e) {
                        return A(t, e)
                    }, function(e) {
                        return I(t, e)
                    })
                }(e, n) : r === C ? I(e, C.error) : void 0 === r ? B(e, n) : t(r) ? function(t, e, n) {
                    u(function(t) {
                        var r = !1,
                            o = function(t, e, n, r) {
                                try {
                                    t.call(e, n, r)
                                } catch (t) {
                                    return t
                                }
                            }(n, e, function(n) {
                                r || (r = !0, e !== n ? A(t, n) : B(t, n))
                            }, function(e) {
                                r || (r = !0, I(t, e))
                            }, t._label);
                        !r && o && (r = !0, I(t, o))
                    }, t)
                }(e, n, r) : B(e, n)
            }

            function A(t, e) {
                t === e ? I(t, new TypeError("You cannot resolve a promise with itself")) : ! function(t) {
                    return "function" == typeof t || "object" == typeof t && null !== t
                }(e) ? B(t, e) : S(t, e, O(e))
            }

            function L(t) {
                t._onerror && t._onerror(t._result), P(t)
            }

            function B(t, e) {
                t._state === T && (t._result = e, t._state = x, 0 !== t._subscribers.length && u(P, t))
            }

            function I(t, e) {
                t._state === T && (t._state = E, t._result = e, u(L, t))
            }

            function j(t, e, n, r) {
                var o = t._subscribers,
                    i = o.length;
                t._onerror = null, o[i] = e, o[i + x] = n, o[i + E] = r, 0 === i && t._state && u(P, t)
            }

            function P(t) {
                var e = t._subscribers,
                    n = t._state;
                if (0 !== e.length) {
                    for (var r = void 0, o = void 0, i = t._result, a = 0; a < e.length; a += 3) r = e[a], o = e[a + n], r ? R(n, r, o, i) : o(i);
                    t._subscribers.length = 0
                }
            }

            function U() {
                this.error = null
            }
            var M = new U;

            function R(e, n, r, o) {
                var i = t(r),
                    a = void 0,
                    s = void 0,
                    u = void 0,
                    c = void 0;
                if (i) {
                    if ((a = function(t, e) {
                            try {
                                return t(e)
                            } catch (t) {
                                return M.error = t, M
                            }
                        }(r, o)) === M ? (c = !0, s = a.error, a = null) : u = !0, n === a) return void I(n, new TypeError("A promises callback cannot return that same promise."))
                } else a = o, u = !0;
                n._state !== T || (i && u ? A(n, a) : c ? I(n, s) : e === x ? B(n, a) : e === E && I(n, a))
            }
            var F = 0;

            function k(t) {
                t[w] = F++, t._state = void 0, t._result = void 0, t._subscribers = []
            }

            function D(t, e) {
                this._instanceConstructor = t, this.promise = new t(_), this.promise[w] || k(this.promise), o(e) ? (this._input = e, this.length = e.length, this._remaining = e.length, this._result = new Array(this.length), 0 === this.length ? B(this.promise, this._result) : (this.length = this.length || 0, this._enumerate(), 0 === this._remaining && B(this.promise, this._result))) : I(this.promise, new Error("Array Methods must be provided an Array"))
            }

            function N(t) {
                this[w] = F++, this._result = this._state = void 0, this._subscribers = [], _ !== t && ("function" != typeof t && function() {
                    throw new TypeError("You must pass a resolver function as the first argument to the promise constructor")
                }(), this instanceof N ? function(t, e) {
                    try {
                        e(function(e) {
                            A(t, e)
                        }, function(e) {
                            I(t, e)
                        })
                    } catch (e) {
                        I(t, e)
                    }
                }(this, t) : function() {
                    throw new TypeError("Failed to construct 'Promise': Please use the 'new' operator, this object constructor cannot be called as a function.")
                }())
            }

            function H() {
                var t = void 0;
                if (void 0 !== r) t = r;
                else if ("undefined" != typeof self) t = self;
                else try {
                    t = Function("return this")()
                } catch (t) {
                    throw new Error("polyfill failed because global object is unavailable in this environment")
                }
                var e = t.Promise;
                if (e) {
                    var n = null;
                    try {
                        n = Object.prototype.toString.call(e.resolve())
                    } catch (t) {}
                    if ("[object Promise]" === n && !e.cast) return
                }
                t.Promise = N
            }
            return D.prototype._enumerate = function() {
                for (var t = this.length, e = this._input, n = 0; this._state === T && n < t; n++) this._eachEntry(e[n], n)
            }, D.prototype._eachEntry = function(t, e) {
                var n = this._instanceConstructor,
                    r = n.resolve;
                if (r === m) {
                    var o = O(t);
                    if (o === b && t._state !== T) this._settledAt(t._state, e, t._result);
                    else if ("function" != typeof o) this._remaining--, this._result[e] = t;
                    else if (n === N) {
                        var i = new n(_);
                        S(i, t, o), this._willSettleAt(i, e)
                    } else this._willSettleAt(new n(function(e) {
                        return e(t)
                    }), e)
                } else this._willSettleAt(r(t), e)
            }, D.prototype._settledAt = function(t, e, n) {
                var r = this.promise;
                r._state === T && (this._remaining--, t === E ? I(r, n) : this._result[e] = n), 0 === this._remaining && B(r, this._result)
            }, D.prototype._willSettleAt = function(t, e) {
                var n = this;
                j(t, void 0, function(t) {
                    return n._settledAt(x, e, t)
                }, function(t) {
                    return n._settledAt(E, e, t)
                })
            }, N.all = function(t) {
                return new D(this, t).promise
            }, N.race = function(t) {
                var e = this;
                return o(t) ? new e(function(n, r) {
                    for (var o = t.length, i = 0; i < o; i++) e.resolve(t[i]).then(n, r)
                }) : new e(function(t, e) {
                    return e(new TypeError("You must pass an array to race."))
                })
            }, N.resolve = m, N.reject = function(t) {
                var e = new this(_);
                return I(e, t), e
            }, N._setScheduler = function(t) {
                s = t
            }, N._setAsap = function(t) {
                u = t
            }, N._asap = u, N.prototype = {
                constructor: N,
                then: b,
                catch: function(t) {
                    return this.then(null, t)
                }
            }, H(), N.polyfill = H, N.Promise = N, N
        })
    }).call(this, n(8), n(9))
}, function(t, e, n) {
    "use strict";
    Object.defineProperty(e, "__esModule", {
        value: !0
    });
    var r = n(11);
    e.OBI_CHAT_ELEMENT_ID = "obi-chat-launcher",
        function() {
            if (document.getElementById(e.OBI_CHAT_ELEMENT_ID)) return "true" === document.getElementById(e.OBI_CHAT_ELEMENT_ID).dataset.config ? void 0 : new r.OBIchat;
            console.log("No element found with id '" + e.OBI_CHAT_ELEMENT_ID + "'")
        }(), e.chat = function(t) {
            return new r.OBIchat(t)
        }
}, function(t, e, n) {
    "use strict";
    Object.defineProperty(e, "__esModule", {
        value: !0
    });
    var r = function() {
        return function(t, e, n, r, o, i, a, s, u, c, l, f, h, d, p, y, g, v, b, m) {
            void 0 === t && (t = ""), void 0 === e && (e = "1.0.0"), void 0 === n && (n = []), void 0 === r && (r = ""), void 0 === o && (o = []), void 0 === i && (i = ""), void 0 === a && (a = ""), void 0 === s && (s = ""), void 0 === u && (u = ""), void 0 === c && (c = !1), void 0 === l && (l = null), void 0 === f && (f = !0), void 0 === h && (h = !1), void 0 === d && (d = ""), void 0 === p && (p = null), void 0 === y && (y = null), void 0 === g && (g = null), void 0 === v && (v = null), void 0 === b && (b = null), void 0 === m && (m = null), this.guid = t, this.version = e, this.preChatFormKeys = n, this.preChatFormText = r, this.emailFormKeys = o, this.emailFormText = i, this.unavailableHeader = a, this.unavailableText = s, this.noAgentsAvailableText = u, this.saveChatHistory = c, this.widgetDisplay = l, this.enableLauncher = f, this.headless = h, this.domainAddress = d, this.widgetStyle = p, this.autoTrigger = y, this.onNewMessage = g, this.onTyping = v, this.onChatInit = b, this.onPreformSubmitted = m, this.widgetStyle = p || null
        }
    }();
    e.CustomerConfig = r
}, function(t, e, n) {
    "use strict";
    Object.defineProperty(e, "__esModule", {
            value: !0
        }),
        function(t) {
            t.NL = "NL", t.EN = "EN", t.DE = "DE", t.FR = "FR"
        }(e.ObiChatLanguages || (e.ObiChatLanguages = {}))
}, function(t, e, n) {
    "use strict";
    Object.defineProperty(e, "__esModule", {
            value: !0
        }),
        function(t) {
            t.BottomRight = "BottomRight", t.BottomLeft = "BottomLeft"
        }(e.ObiChatWidgetPositions || (e.ObiChatWidgetPositions = {}))
}, function(t, e, n) {
    "use strict";
    Object.defineProperty(e, "__esModule", {
        value: !0
    });
    var r, o = n(4);
    e.createIframe = function(t) {
        var e = document.createElement("iframe");
        document.body.appendChild(e), i(e, "obiChatLauncher"), i(e, "obiChatLauncherHidden"), t && t.widgetStyle && t.widgetStyle.widgetText && (i(e, "obiChatLauncher__text"), r = function(t) {
            return "width: " + (7 * t.length + 120) + "px; max-width: 300px;"
        }(t.widgetStyle.widgetText), e.style.cssText = r);
        return e.setAttribute("src", "about:blank"), e.contentWindow.document.open("text/html", "replace"), e.contentWindow.document.write("<!DOCTYPE html><html><body><script src=obi-chat.bundle.1.0.0.min.js><\/script></body></html>"), e.contentWindow.document.close(), e
    }, e.openLauncher = function(t, n, o) {
        o && (t ? (i(o, "obiChatLauncherOpen"), e.removeClassFromIframe(o, "obiChatLauncherHidden"), o.removeAttribute("style")) : (e.removeClassFromIframe(o, "obiChatLauncherOpen"), o.style.cssText = r, n.enableLauncher && !n.headless || i(o, "obiChatLauncherHidden")))
    }, e.positionIframe = function(t, e) {
        switch (e && e.widgetStyle ? e.widgetStyle.widgetPosition : null) {
            case o.ObiChatWidgetPositions.BottomLeft:
                i(t, "obiChatLauncherBottomLeft");
                break;
            case o.ObiChatWidgetPositions.BottomRight:
            default:
                i(t, "obiChatLauncherBottomRight")
        }
    }, e.removeClassFromIframe = function(t, e) {
        t.classList.remove(e)
    };
    var i = function(t, e) {
        t.classList.add(e)
    }
}, function(t, e, n) {
    n(7), t.exports = n(1)
}, function(t, e, n) {
    "use strict";
    n.r(e),
        function(t) {
            n.d(e, "Headers", function() {
                return c
            }), n.d(e, "Request", function() {
                return g
            }), n.d(e, "Response", function() {
                return b
            }), n.d(e, "DOMException", function() {
                return w
            }), n.d(e, "fetch", function() {
                return _
            });
            var r = {
                searchParams: "URLSearchParams" in self,
                iterable: "Symbol" in self && "iterator" in Symbol,
                blob: "FileReader" in self && "Blob" in self && function() {
                    try {
                        return new Blob, !0
                    } catch (t) {
                        return !1
                    }
                }(),
                formData: "FormData" in self,
                arrayBuffer: "ArrayBuffer" in self
            };
            if (r.arrayBuffer) var o = ["[object Int8Array]", "[object Uint8Array]", "[object Uint8ClampedArray]", "[object Int16Array]", "[object Uint16Array]", "[object Int32Array]", "[object Uint32Array]", "[object Float32Array]", "[object Float64Array]"],
                i = ArrayBuffer.isView || function(t) {
                    return t && o.indexOf(Object.prototype.toString.call(t)) > -1
                };

            function a(t) {
                if ("string" != typeof t && (t = String(t)), /[^a-z0-9\-#$%&'*+.^_`|~]/i.test(t)) throw new TypeError("Invalid character in header field name");
                return t.toLowerCase()
            }

            function s(t) {
                return "string" != typeof t && (t = String(t)), t
            }

            function u(t) {
                var e = {
                    next: function() {
                        var e = t.shift();
                        return {
                            done: void 0 === e,
                            value: e
                        }
                    }
                };
                return r.iterable && (e[Symbol.iterator] = function() {
                    return e
                }), e
            }

            function c(t) {
                this.map = {}, t instanceof c ? t.forEach(function(t, e) {
                    this.append(e, t)
                }, this) : Array.isArray(t) ? t.forEach(function(t) {
                    this.append(t[0], t[1])
                }, this) : t && Object.getOwnPropertyNames(t).forEach(function(e) {
                    this.append(e, t[e])
                }, this)
            }

            function l(e) {
                if (e.bodyUsed) return t.reject(new TypeError("Already read"));
                e.bodyUsed = !0
            }

            function f(e) {
                return new t(function(t, n) {
                    e.onload = function() {
                        t(e.result)
                    }, e.onerror = function() {
                        n(e.error)
                    }
                })
            }

            function h(t) {
                var e = new FileReader,
                    n = f(e);
                return e.readAsArrayBuffer(t), n
            }

            function d(t) {
                if (t.slice) return t.slice(0);
                var e = new Uint8Array(t.byteLength);
                return e.set(new Uint8Array(t)), e.buffer
            }

            function p() {
                return this.bodyUsed = !1, this._initBody = function(t) {
                    this._bodyInit = t, t ? "string" == typeof t ? this._bodyText = t : r.blob && Blob.prototype.isPrototypeOf(t) ? this._bodyBlob = t : r.formData && FormData.prototype.isPrototypeOf(t) ? this._bodyFormData = t : r.searchParams && URLSearchParams.prototype.isPrototypeOf(t) ? this._bodyText = t.toString() : r.arrayBuffer && r.blob && function(t) {
                        return t && DataView.prototype.isPrototypeOf(t)
                    }(t) ? (this._bodyArrayBuffer = d(t.buffer), this._bodyInit = new Blob([this._bodyArrayBuffer])) : r.arrayBuffer && (ArrayBuffer.prototype.isPrototypeOf(t) || i(t)) ? this._bodyArrayBuffer = d(t) : this._bodyText = t = Object.prototype.toString.call(t) : this._bodyText = "", this.headers.get("content-type") || ("string" == typeof t ? this.headers.set("content-type", "text/plain;charset=UTF-8") : this._bodyBlob && this._bodyBlob.type ? this.headers.set("content-type", this._bodyBlob.type) : r.searchParams && URLSearchParams.prototype.isPrototypeOf(t) && this.headers.set("content-type", "application/x-www-form-urlencoded;charset=UTF-8"))
                }, r.blob && (this.blob = function() {
                    var e = l(this);
                    if (e) return e;
                    if (this._bodyBlob) return t.resolve(this._bodyBlob);
                    if (this._bodyArrayBuffer) return t.resolve(new Blob([this._bodyArrayBuffer]));
                    if (this._bodyFormData) throw new Error("could not read FormData body as blob");
                    return t.resolve(new Blob([this._bodyText]))
                }, this.arrayBuffer = function() {
                    return this._bodyArrayBuffer ? l(this) || t.resolve(this._bodyArrayBuffer) : this.blob().then(h)
                }), this.text = function() {
                    var e = l(this);
                    if (e) return e;
                    if (this._bodyBlob) return function(t) {
                        var e = new FileReader,
                            n = f(e);
                        return e.readAsText(t), n
                    }(this._bodyBlob);
                    if (this._bodyArrayBuffer) return t.resolve(function(t) {
                        for (var e = new Uint8Array(t), n = new Array(e.length), r = 0; r < e.length; r++) n[r] = String.fromCharCode(e[r]);
                        return n.join("")
                    }(this._bodyArrayBuffer));
                    if (this._bodyFormData) throw new Error("could not read FormData body as text");
                    return t.resolve(this._bodyText)
                }, r.formData && (this.formData = function() {
                    return this.text().then(v)
                }), this.json = function() {
                    return this.text().then(JSON.parse)
                }, this
            }
            c.prototype.append = function(t, e) {
                t = a(t), e = s(e);
                var n = this.map[t];
                this.map[t] = n ? n + ", " + e : e
            }, c.prototype.delete = function(t) {
                delete this.map[a(t)]
            }, c.prototype.get = function(t) {
                return t = a(t), this.has(t) ? this.map[t] : null
            }, c.prototype.has = function(t) {
                return this.map.hasOwnProperty(a(t))
            }, c.prototype.set = function(t, e) {
                this.map[a(t)] = s(e)
            }, c.prototype.forEach = function(t, e) {
                for (var n in this.map) this.map.hasOwnProperty(n) && t.call(e, this.map[n], n, this)
            }, c.prototype.keys = function() {
                var t = [];
                return this.forEach(function(e, n) {
                    t.push(n)
                }), u(t)
            }, c.prototype.values = function() {
                var t = [];
                return this.forEach(function(e) {
                    t.push(e)
                }), u(t)
            }, c.prototype.entries = function() {
                var t = [];
                return this.forEach(function(e, n) {
                    t.push([n, e])
                }), u(t)
            }, r.iterable && (c.prototype[Symbol.iterator] = c.prototype.entries);
            var y = ["DELETE", "GET", "HEAD", "OPTIONS", "POST", "PUT"];

            function g(t, e) {
                var n = (e = e || {}).body;
                if (t instanceof g) {
                    if (t.bodyUsed) throw new TypeError("Already read");
                    this.url = t.url, this.credentials = t.credentials, e.headers || (this.headers = new c(t.headers)), this.method = t.method, this.mode = t.mode, this.signal = t.signal, n || null == t._bodyInit || (n = t._bodyInit, t.bodyUsed = !0)
                } else this.url = String(t);
                if (this.credentials = e.credentials || this.credentials || "same-origin", !e.headers && this.headers || (this.headers = new c(e.headers)), this.method = function(t) {
                        var e = t.toUpperCase();
                        return y.indexOf(e) > -1 ? e : t
                    }(e.method || this.method || "GET"), this.mode = e.mode || this.mode || null, this.signal = e.signal || this.signal, this.referrer = null, ("GET" === this.method || "HEAD" === this.method) && n) throw new TypeError("Body not allowed for GET or HEAD requests");
                this._initBody(n)
            }

            function v(t) {
                var e = new FormData;
                return t.trim().split("&").forEach(function(t) {
                    if (t) {
                        var n = t.split("="),
                            r = n.shift().replace(/\+/g, " "),
                            o = n.join("=").replace(/\+/g, " ");
                        e.append(decodeURIComponent(r), decodeURIComponent(o))
                    }
                }), e
            }

            function b(t, e) {
                e || (e = {}), this.type = "default", this.status = void 0 === e.status ? 200 : e.status, this.ok = this.status >= 200 && this.status < 300, this.statusText = "statusText" in e ? e.statusText : "OK", this.headers = new c(e.headers), this.url = e.url || "", this._initBody(t)
            }
            g.prototype.clone = function() {
                return new g(this, {
                    body: this._bodyInit
                })
            }, p.call(g.prototype), p.call(b.prototype), b.prototype.clone = function() {
                return new b(this._bodyInit, {
                    status: this.status,
                    statusText: this.statusText,
                    headers: new c(this.headers),
                    url: this.url
                })
            }, b.error = function() {
                var t = new b(null, {
                    status: 0,
                    statusText: ""
                });
                return t.type = "error", t
            };
            var m = [301, 302, 303, 307, 308];
            b.redirect = function(t, e) {
                if (-1 === m.indexOf(e)) throw new RangeError("Invalid status code");
                return new b(null, {
                    status: e,
                    headers: {
                        location: t
                    }
                })
            };
            var w = self.DOMException;
            try {
                new w
            } catch (t) {
                (w = function(t, e) {
                    this.message = t, this.name = e;
                    var n = Error(t);
                    this.stack = n.stack
                }).prototype = Object.create(Error.prototype), w.prototype.constructor = w
            }

            function _(e, n) {
                return new t(function(t, o) {
                    var i = new g(e, n);
                    if (i.signal && i.signal.aborted) return o(new w("Aborted", "AbortError"));
                    var a = new XMLHttpRequest;

                    function s() {
                        a.abort()
                    }
                    a.onload = function() {
                        var e = {
                            status: a.status,
                            statusText: a.statusText,
                            headers: function(t) {
                                var e = new c;
                                return t.replace(/\r?\n[\t ]+/g, " ").split(/\r?\n/).forEach(function(t) {
                                    var n = t.split(":"),
                                        r = n.shift().trim();
                                    if (r) {
                                        var o = n.join(":").trim();
                                        e.append(r, o)
                                    }
                                }), e
                            }(a.getAllResponseHeaders() || "")
                        };
                        e.url = "responseURL" in a ? a.responseURL : e.headers.get("X-Request-URL");
                        var n = "response" in a ? a.response : a.responseText;
                        t(new b(n, e))
                    }, a.onerror = function() {
                        o(new TypeError("Network request failed"))
                    }, a.ontimeout = function() {
                        o(new TypeError("Network request failed"))
                    }, a.onabort = function() {
                        o(new w("Aborted", "AbortError"))
                    }, a.open(i.method, i.url, !0), "include" === i.credentials ? a.withCredentials = !0 : "omit" === i.credentials && (a.withCredentials = !1), "responseType" in a && r.blob && (a.responseType = "blob"), i.headers.forEach(function(t, e) {
                        a.setRequestHeader(e, t)
                    }), i.signal && (i.signal.addEventListener("abort", s), a.onreadystatechange = function() {
                        4 === a.readyState && i.signal.removeEventListener("abort", s)
                    }), a.send(void 0 === i._bodyInit ? null : i._bodyInit)
                })
            }
            _.polyfill = !0, self.fetch || (self.fetch = _, self.Headers = c, self.Request = g, self.Response = b)
        }.call(this, n(0))
}, function(t, e) {
    var n, r, o = t.exports = {};

    function i() {
        throw new Error("setTimeout has not been defined")
    }

    function a() {
        throw new Error("clearTimeout has not been defined")
    }

    function s(t) {
        if (n === setTimeout) return setTimeout(t, 0);
        if ((n === i || !n) && setTimeout) return n = setTimeout, setTimeout(t, 0);
        try {
            return n(t, 0)
        } catch (e) {
            try {
                return n.call(null, t, 0)
            } catch (e) {
                return n.call(this, t, 0)
            }
        }
    }! function() {
        try {
            n = "function" == typeof setTimeout ? setTimeout : i
        } catch (t) {
            n = i
        }
        try {
            r = "function" == typeof clearTimeout ? clearTimeout : a
        } catch (t) {
            r = a
        }
    }();
    var u, c = [],
        l = !1,
        f = -1;

    function h() {
        l && u && (l = !1, u.length ? c = u.concat(c) : f = -1, c.length && d())
    }

    function d() {
        if (!l) {
            var t = s(h);
            l = !0;
            for (var e = c.length; e;) {
                for (u = c, c = []; ++f < e;) u && u[f].run();
                f = -1, e = c.length
            }
            u = null, l = !1,
                function(t) {
                    if (r === clearTimeout) return clearTimeout(t);
                    if ((r === a || !r) && clearTimeout) return r = clearTimeout, clearTimeout(t);
                    try {
                        r(t)
                    } catch (e) {
                        try {
                            return r.call(null, t)
                        } catch (e) {
                            return r.call(this, t)
                        }
                    }
                }(t)
        }
    }

    function p(t, e) {
        this.fun = t, this.array = e
    }

    function y() {}
    o.nextTick = function(t) {
        var e = new Array(arguments.length - 1);
        if (arguments.length > 1)
            for (var n = 1; n < arguments.length; n++) e[n - 1] = arguments[n];
        c.push(new p(t, e)), 1 !== c.length || l || s(d)
    }, p.prototype.run = function() {
        this.fun.apply(null, this.array)
    }, o.title = "browser", o.browser = !0, o.env = {}, o.argv = [], o.version = "", o.versions = {}, o.on = y, o.addListener = y, o.once = y, o.off = y, o.removeListener = y, o.removeAllListeners = y, o.emit = y, o.prependListener = y, o.prependOnceListener = y, o.listeners = function(t) {
        return []
    }, o.binding = function(t) {
        throw new Error("process.binding is not supported")
    }, o.cwd = function() {
        return "/"
    }, o.chdir = function(t) {
        throw new Error("process.chdir is not supported")
    }, o.umask = function() {
        return 0
    }
}, function(t, e) {
    var n;
    n = function() {
        return this
    }();
    try {
        n = n || Function("return this")() || (0, eval)("this")
    } catch (t) {
        "object" == typeof window && (n = window)
    }
    t.exports = n
}, function(t, e) {}, function(t, e, n) {
    "use strict";
    (function(t) {
        var r = this && this.__awaiter || function(e, n, r, o) {
                return new(r || (r = t))(function(t, i) {
                    function a(t) {
                        try {
                            u(o.next(t))
                        } catch (t) {
                            i(t)
                        }
                    }

                    function s(t) {
                        try {
                            u(o.throw(t))
                        } catch (t) {
                            i(t)
                        }
                    }

                    function u(e) {
                        e.done ? t(e.value) : new r(function(t) {
                            t(e.value)
                        }).then(a, s)
                    }
                    u((o = o.apply(e, n || [])).next())
                })
            },
            o = this && this.__generator || function(t, e) {
                var n, r, o, i, a = {
                    label: 0,
                    sent: function() {
                        if (1 & o[0]) throw o[1];
                        return o[1]
                    },
                    trys: [],
                    ops: []
                };
                return i = {
                    next: s(0),
                    throw: s(1),
                    return: s(2)
                }, "function" == typeof Symbol && (i[Symbol.iterator] = function() {
                    return this
                }), i;

                function s(i) {
                    return function(s) {
                        return function(i) {
                            if (n) throw new TypeError("Generator is already executing.");
                            for (; a;) try {
                                if (n = 1, r && (o = 2 & i[0] ? r.return : i[0] ? r.throw || ((o = r.return) && o.call(r), 0) : r.next) && !(o = o.call(r, i[1])).done) return o;
                                switch (r = 0, o && (i = [2 & i[0], o.value]), i[0]) {
                                    case 0:
                                    case 1:
                                        o = i;
                                        break;
                                    case 4:
                                        return a.label++, {
                                            value: i[1],
                                            done: !1
                                        };
                                    case 5:
                                        a.label++, r = i[1], i = [0];
                                        continue;
                                    case 7:
                                        i = a.ops.pop(), a.trys.pop();
                                        continue;
                                    default:
                                        if (!(o = (o = a.trys).length > 0 && o[o.length - 1]) && (6 === i[0] || 2 === i[0])) {
                                            a = 0;
                                            continue
                                        }
                                        if (3 === i[0] && (!o || i[1] > o[0] && i[1] < o[3])) {
                                            a.label = i[1];
                                            break
                                        }
                                        if (6 === i[0] && a.label < o[1]) {
                                            a.label = o[1], o = i;
                                            break
                                        }
                                        if (o && a.label < o[2]) {
                                            a.label = o[2], a.ops.push(i);
                                            break
                                        }
                                        o[2] && a.ops.pop(), a.trys.pop();
                                        continue
                                }
                                i = e.call(t, a)
                            } catch (t) {
                                i = [6, t], r = 0
                            } finally {
                                n = o = 0
                            }
                            if (5 & i[0]) throw i[1];
                            return {
                                value: i[0] ? i[1] : void 0,
                                done: !0
                            }
                        }([i, s])
                    }
                }
            };
        Object.defineProperty(e, "__esModule", {
            value: !0
        });
        var i = n(2),
            a = n(3),
            s = n(12),
            u = n(1),
            c = n(13),
            l = n(14),
            f = n(5),
            h = n(17);
        n(19);
        var d = function() {
            function t(t) {
                void 0 === t && (t = new i.CustomerConfig), this.customerConfig = t, this.open = this.openChat, this.isAvailable = this.isAgentAvailable, this.sendContextMessage = this.onSendContextMessage, this.sendUserMessage = this.onSendUserMessage, this.setLanguage = this.onSetLanguage, this.init()
            }
            return t.prototype.init = function() {
                return r(this, void 0, void 0, function() {
                    return o(this, function(t) {
                        switch (t.label) {
                            case 0:
                                return [4, this.prepareChat(this.customerConfig)];
                            case 1:
                                return t.sent(), [2]
                        }
                    })
                })
            }, t.prototype.prepareChat = function(t) {
                return r(this, void 0, void 0, function() {
                    var e, n;
                    return o(this, function(r) {
                        switch (r.label) {
                            case 0:
                                return this.guid = document.getElementById(u.OBI_CHAT_ELEMENT_ID).dataset.guid, e = this, [4, l.fetchConfig(t, this.guid)];
                            case 1:
                                return e.config = r.sent(), [4, c.agentAvailable(this.guid)];
                            case 2:
                                return n = r.sent(), n && !0 === n.available || this.config.widgetDisplay !== s.OBIWidgetDisplay.hide || this.config.headless ? (this.initChat(), [2]) : [2]
                        }
                    })
                })
            }, t.prototype.initChat = function() {
                this.iframe || (this.iframe = f.createIframe(this.config), window.addEventListener("message", h.receiveIframeMessage.bind(null, this.iframe, this.config, this.customerConfig)), f.positionIframe(this.iframe, this.config), f.openLauncher(!1, this.config, this.iframe), this.listenToUrlChanges(this.iframe))
            }, t.prototype.openChat = function() {
                this.initChat(), f.openLauncher(!0, this.config, this.iframe), h.onOpen(this.iframe, this.config)
            }, t.prototype.onSendContextMessage = function(t) {
                h.onSendContextMessage(this.iframe, t)
            }, t.prototype.onSendUserMessage = function(t) {
                t.author || (t.author = {
                    url: null,
                    name: null
                }), t.displayInWidget = !0, t.url = window.location.href, h.onSendUserMessage(this.iframe, t)
            }, t.prototype.onSetLanguage = function(t) {
                switch (t) {
                    case a.ObiChatLanguages.DE:
                    case a.ObiChatLanguages.EN:
                    case a.ObiChatLanguages.FR:
                    case a.ObiChatLanguages.NL:
                        h.onUpdateLanguage(this.iframe, t);
                        break;
                    default:
                        console.error('Unknown language. Please select "DE", "EN", "FR" or "NL"')
                }
            }, t.prototype.isAgentAvailable = function() {
                return r(this, void 0, void 0, function() {
                    return o(this, function(t) {
                        return [2, c.agentAvailable(this.guid)]
                    })
                })
            }, t.prototype.listenToUrlChanges = function(t) {
                var e = history.pushState;
                window.history.pushState = function() {
                    e.apply(history, arguments), h.onPageUrl(t, window.location.href)
                }
            }, t
        }();
        e.OBIchat = d
    }).call(this, n(0))
}, function(t, e, n) {
    "use strict";
    Object.defineProperty(e, "__esModule", {
            value: !0
        }),
        function(t) {
            t.hide = "hide", t.emailForm = "emailForm"
        }(e.OBIWidgetDisplay || (e.OBIWidgetDisplay = {}))
}, function(t, e, n) {
    "use strict";
    (function(t) {
        var n = this && this.__awaiter || function(e, n, r, o) {
                return new(r || (r = t))(function(t, i) {
                    function a(t) {
                        try {
                            u(o.next(t))
                        } catch (t) {
                            i(t)
                        }
                    }

                    function s(t) {
                        try {
                            u(o.throw(t))
                        } catch (t) {
                            i(t)
                        }
                    }

                    function u(e) {
                        e.done ? t(e.value) : new r(function(t) {
                            t(e.value)
                        }).then(a, s)
                    }
                    u((o = o.apply(e, n || [])).next())
                })
            },
            r = this && this.__generator || function(t, e) {
                var n, r, o, i, a = {
                    label: 0,
                    sent: function() {
                        if (1 & o[0]) throw o[1];
                        return o[1]
                    },
                    trys: [],
                    ops: []
                };
                return i = {
                    next: s(0),
                    throw: s(1),
                    return: s(2)
                }, "function" == typeof Symbol && (i[Symbol.iterator] = function() {
                    return this
                }), i;

                function s(i) {
                    return function(s) {
                        return function(i) {
                            if (n) throw new TypeError("Generator is already executing.");
                            for (; a;) try {
                                if (n = 1, r && (o = 2 & i[0] ? r.return : i[0] ? r.throw || ((o = r.return) && o.call(r), 0) : r.next) && !(o = o.call(r, i[1])).done) return o;
                                switch (r = 0, o && (i = [2 & i[0], o.value]), i[0]) {
                                    case 0:
                                    case 1:
                                        o = i;
                                        break;
                                    case 4:
                                        return a.label++, {
                                            value: i[1],
                                            done: !1
                                        };
                                    case 5:
                                        a.label++, r = i[1], i = [0];
                                        continue;
                                    case 7:
                                        i = a.ops.pop(), a.trys.pop();
                                        continue;
                                    default:
                                        if (!(o = (o = a.trys).length > 0 && o[o.length - 1]) && (6 === i[0] || 2 === i[0])) {
                                            a = 0;
                                            continue
                                        }
                                        if (3 === i[0] && (!o || i[1] > o[0] && i[1] < o[3])) {
                                            a.label = i[1];
                                            break
                                        }
                                        if (6 === i[0] && a.label < o[1]) {
                                            a.label = o[1], o = i;
                                            break
                                        }
                                        if (o && a.label < o[2]) {
                                            a.label = o[2], a.ops.push(i);
                                            break
                                        }
                                        o[2] && a.ops.pop(), a.trys.pop();
                                        continue
                                }
                                i = e.call(t, a)
                            } catch (t) {
                                i = [6, t], r = 0
                            } finally {
                                n = o = 0
                            }
                            if (5 & i[0]) throw i[1];
                            return {
                                value: i[0] ? i[1] : void 0,
                                done: !0
                            }
                        }([i, s])
                    }
                }
            },
            o = this;
        Object.defineProperty(e, "__esModule", {
            value: !0
        }), e.agentAvailable = function(t) {
            return n(o, void 0, void 0, function() {
                return r(this, function(e) {
                    switch (e.label) {
                        case 0:
                            return [4, fetch("https://cloudstatic.obi4wan.com/api/v1.0/chat/availability/" + t)];
                        case 1:
                            return [4, e.sent().json()];
                        case 2:
                            return [2, e.sent()]
                    }
                })
            })
        }
    }).call(this, n(0))
}, function(t, e, n) {
    "use strict";
    (function(t) {
        var r = this && this.__awaiter || function(e, n, r, o) {
                return new(r || (r = t))(function(t, i) {
                    function a(t) {
                        try {
                            u(o.next(t))
                        } catch (t) {
                            i(t)
                        }
                    }

                    function s(t) {
                        try {
                            u(o.throw(t))
                        } catch (t) {
                            i(t)
                        }
                    }

                    function u(e) {
                        e.done ? t(e.value) : new r(function(t) {
                            t(e.value)
                        }).then(a, s)
                    }
                    u((o = o.apply(e, n || [])).next())
                })
            },
            o = this && this.__generator || function(t, e) {
                var n, r, o, i, a = {
                    label: 0,
                    sent: function() {
                        if (1 & o[0]) throw o[1];
                        return o[1]
                    },
                    trys: [],
                    ops: []
                };
                return i = {
                    next: s(0),
                    throw: s(1),
                    return: s(2)
                }, "function" == typeof Symbol && (i[Symbol.iterator] = function() {
                    return this
                }), i;

                function s(i) {
                    return function(s) {
                        return function(i) {
                            if (n) throw new TypeError("Generator is already executing.");
                            for (; a;) try {
                                if (n = 1, r && (o = 2 & i[0] ? r.return : i[0] ? r.throw || ((o = r.return) && o.call(r), 0) : r.next) && !(o = o.call(r, i[1])).done) return o;
                                switch (r = 0, o && (i = [2 & i[0], o.value]), i[0]) {
                                    case 0:
                                    case 1:
                                        o = i;
                                        break;
                                    case 4:
                                        return a.label++, {
                                            value: i[1],
                                            done: !1
                                        };
                                    case 5:
                                        a.label++, r = i[1], i = [0];
                                        continue;
                                    case 7:
                                        i = a.ops.pop(), a.trys.pop();
                                        continue;
                                    default:
                                        if (!(o = (o = a.trys).length > 0 && o[o.length - 1]) && (6 === i[0] || 2 === i[0])) {
                                            a = 0;
                                            continue
                                        }
                                        if (3 === i[0] && (!o || i[1] > o[0] && i[1] < o[3])) {
                                            a.label = i[1];
                                            break
                                        }
                                        if (6 === i[0] && a.label < o[1]) {
                                            a.label = o[1], o = i;
                                            break
                                        }
                                        if (o && a.label < o[2]) {
                                            a.label = o[2], a.ops.push(i);
                                            break
                                        }
                                        o[2] && a.ops.pop(), a.trys.pop();
                                        continue
                                }
                                i = e.call(t, a)
                            } catch (t) {
                                i = [6, t], r = 0
                            } finally {
                                n = o = 0
                            }
                            if (5 & i[0]) throw i[1];
                            return {
                                value: i[0] ? i[1] : void 0,
                                done: !0
                            }
                        }([i, s])
                    }
                }
            },
            i = this;
        Object.defineProperty(e, "__esModule", {
            value: !0
        });
        var a = n(15),
            s = n(2),
            u = n(16);
        e.fetchConfig = function(t, e) {
            return r(i, void 0, void 0, function() {
                var n;
                return o(this, function(r) {
                    switch (r.label) {
                        case 0:
                            return [4, fetch("https://cloudstatic.obi4wan.com/api/v1.0/chat/configuration/" + e)];
                        case 1:
                            return [4, r.sent().json()];
                        case 2:
                            return n = r.sent(), [2, new s.CustomerConfig(n.guid, "1.0.0", t.preChatFormKeys && t.preChatFormKeys.length > 0 ? t.preChatFormKeys : n.preChatFormKeys, t.preChatFormText || n.preChatFormText, t.emailFormKeys && t.emailFormKeys.length > 0 ? t.emailFormKeys : n.emailFormKeys, t.emailFormText || n.emailFormText, t.unavailableHeader || n.unavailableHeader, t.unavailableText || n.unavailableText, t.noAgentsAvailableText || n.noAgentsAvailableText, t.saveChatHistory || n.saveChatHistory, t.widgetDisplay || n.widgetDisplay, t.enableLauncher, t.headless, window.location.href, new u.WidgetStyle(t.widgetStyle && t.widgetStyle.widgetText || n.widgetStyle && n.widgetStyle.widgetText, t.widgetStyle && t.widgetStyle.title || n.widgetStyle && n.widgetStyle.title, t.widgetStyle && t.widgetStyle.subtitle || n.widgetStyle && n.widgetStyle.subtitle, t.widgetStyle && t.widgetStyle.welcomeLine || n.widgetStyle && n.widgetStyle.welcomeLine, t.widgetStyle && t.widgetStyle.color || n.widgetStyle && n.widgetStyle.color, t.widgetStyle && t.widgetStyle.logoUri || n.widgetStyle && n.widgetStyle.logoUri, t.widgetStyle && t.widgetStyle.language || n.widgetStyle && n.widgetStyle.language, t.widgetStyle && t.widgetStyle.widgetPosition || n.widgetStyle && n.widgetStyle.widgetPosition), new a.AutoTrigger(t.autoTrigger && t.autoTrigger.enabled || n.autoTrigger && n.autoTrigger.enabled, t.autoTrigger && t.autoTrigger.delaySeconds || n.autoTrigger && n.autoTrigger.delaySeconds, t.autoTrigger && t.autoTrigger.text || n.autoTrigger && n.autoTrigger.text, t.autoTrigger && t.autoTrigger.cooldownTime || n.autoTrigger && n.autoTrigger.cooldownTime, t.autoTrigger && t.autoTrigger.triggerOnlyOnce || n.autoTrigger && n.autoTrigger.triggerOnlyOnce))]
                    }
                })
            })
        }
    }).call(this, n(0))
}, function(t, e, n) {
    "use strict";
    Object.defineProperty(e, "__esModule", {
        value: !0
    });
    var r = function() {
        return function(t, e, n, r, o) {
            void 0 === t && (t = !1), void 0 === e && (e = 0), void 0 === n && (n = ""), void 0 === r && (r = 0), void 0 === o && (o = !1), this.enabled = t, this.delaySeconds = e, this.text = n, this.cooldownTime = r, this.triggerOnlyOnce = o
        }
    }();
    e.AutoTrigger = r
}, function(t, e, n) {
    "use strict";
    Object.defineProperty(e, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = n(4),
        i = function() {
            return function(t, e, n, i, a, s, u, c) {
                void 0 === t && (t = ""), void 0 === e && (e = ""), void 0 === n && (n = ""), void 0 === i && (i = ""), void 0 === a && (a = ""), void 0 === s && (s = ""), void 0 === u && (u = r.ObiChatLanguages.NL), void 0 === c && (c = o.ObiChatWidgetPositions.BottomRight), this.widgetText = t, this.title = e, this.subtitle = n, this.welcomeLine = i, this.color = a, this.logoUri = s, this.language = u, this.widgetPosition = c
            }
        }();
    e.WidgetStyle = i
}, function(t, e, n) {
    "use strict";
    Object.defineProperty(e, "__esModule", {
        value: !0
    });
    var r = n(18),
        o = n(5);
    e.receiveIframeMessage = function(t, n, a, s) {
        var u = s.data;
        switch (u.type) {
            case r.OBIEventType.ConfigEvent:
                var c = s.data.data.forceStartPusherConnection;
                i(t, n, c);
                break;
            case r.OBIEventType.OpenChatEvent:
                o.openLauncher(!0, n, t);
                break;
            case r.OBIEventType.CloseChatEvent:
                o.openLauncher(!1, n, t);
                break;
            case r.OBIEventType.MessageEvent:
                a && a.onNewMessage && a.onNewMessage(JSON.parse(u.data.message));
                break;
            case r.OBIEventType.TypingEvent:
                a && a.onTyping && a.onTyping(u.data);
                break;
            case r.OBIEventType.OnChatInit:
                n && (n.enableLauncher && !n.headless && o.removeClassFromIframe(t, "obiChatLauncherHidden"), a.onChatInit && i(t, n, !0));
                break;
            case r.OBIEventType.PusherConnected:
                a.onChatInit && a.onChatInit();
                break;
            case r.OBIEventType.PageChangeEvent:
                e.onPageUrl(t, window.location.href);
                break;
            case r.OBIEventType.OnPreformSubmitted:
                a.onPreformSubmitted && a.onPreformSubmitted()
        }
    }, e.onOpen = function(t, e) {
        a(t, {
            type: r.OBIEventType.OpenChatEvent
        }), e && i(t, e, !0)
    }, e.onUpdateLanguage = function(t, e) {
        a(t, {
            type: r.OBIEventType.LanguageUpdateEvent,
            data: e
        })
    }, e.onSendContextMessage = function(t, e) {
        a(t, {
            type: r.OBIEventType.ContextMessageEvent,
            data: e
        })
    }, e.onSendUserMessage = function(t, e) {
        a(t, {
            type: r.OBIEventType.MessageEvent,
            data: e
        })
    }, e.onPageUrl = function(t, e) {
        a(t, {
            type: r.OBIEventType.PageChangeEvent,
            data: e
        })
    };
    var i = function(t, e, n) {
            a(t, {
                config: e,
                forcePusherStart: n
            })
        },
        a = function(t, e) {
            t.contentWindow.postMessage(e, "*")
        }
}, function(t, e, n) {
    "use strict";
    Object.defineProperty(e, "__esModule", {
            value: !0
        }),
        function(t) {
            t.MessageEvent = "client-message", t.TypingEvent = "client-typing", t.EmailFormEvent = "client-emailform", t.OpenChatEvent = "client-open-chat", t.CloseChatEvent = "client-close-chat", t.ContextMessageEvent = "client-context-message", t.ConfigEvent = "client-get-config", t.LanguageUpdateEvent = "client-language-update", t.OnChatInit = "client-on-chat-init", t.PageChangeEvent = "client-page-change", t.PageHistoryEvent = "client-page-history", t.AutoTriggerEvent = "client-auto-trigger", t.OnPreformSubmitted = "client-on-preform-submitted", t.PusherConnected = "client-pusher-connected"
        }(e.OBIEventType || (e.OBIEventType = {}))
}, function(t, e, n) {
    var r = n(20);
    "string" == typeof r && (r = [
        [t.i, r, ""]
    ]);
    var o = {
        hmr: !0,
        transform: void 0,
        insertInto: void 0
    };
    n(22)(r, o);
    r.locals && (t.exports = r.locals)
}, function(t, e, n) {
    (t.exports = n(21)(!1)).push([t.i, ".obiChatLauncher {\r\n    width: 40px;\r\n    height: 35px;\r\n    position: fixed;\r\n    border-width: 0;\r\n    background: transparent;\r\n    border-radius: 10px;\r\n    box-shadow: 0 5px 40px rgba(0,0,0,.3);\r\n    justify-content: center;\r\n    z-index: 2147483647;\r\n}\r\n\r\n.obiChatLauncherBottomRight {\r\n    bottom: 20px;\r\n    right: 20px;\r\n}\r\n\r\n.obiChatLauncherBottomLeft {\r\n    bottom: 20px;\r\n    left: 20px;\r\n}\r\n\r\n.obiChatLauncher__text {\r\n    width: 300px;\r\n}\r\n\r\n.obiChatLauncherOpen {\r\n    height: calc(100% - 30px);\r\n    max-height: 560px;\r\n    min-height: 340px;\r\n    width: 390px;\r\n}\r\n\r\n.obiChatLauncherHidden {\r\n    display: none;\r\n}\r\n\r\n@media only screen and (max-device-width: 667px), screen and (max-width: 450px) {\r\n    .obiChatLauncherOpen {\r\n        width: 100%;\r\n        height: 100%;\r\n        max-height: 100%;\r\n        top: 0;\r\n        left:0;\r\n    }\r\n\r\n    .obiChatLauncher {\r\n        box-shadow: none;\r\n    }\r\n\r\n    .obiChatLauncherBottomRight, \r\n    .obiChatLauncherBottomLeft {\r\n        bottom: 10px;\r\n    }\r\n}\r\n", ""])
}, function(t, e) {
    t.exports = function(t) {
        var e = [];
        return e.toString = function() {
            return this.map(function(e) {
                var n = function(t, e) {
                    var n = t[1] || "",
                        r = t[3];
                    if (!r) return n;
                    if (e && "function" == typeof btoa) {
                        var o = function(t) {
                                return "/*# sourceMappingURL=data:application/json;charset=utf-8;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(t)))) + " */"
                            }(r),
                            i = r.sources.map(function(t) {
                                return "/*# sourceURL=" + r.sourceRoot + t + " */"
                            });
                        return [n].concat(i).concat([o]).join("\n")
                    }
                    return [n].join("\n")
                }(e, t);
                return e[2] ? "@media " + e[2] + "{" + n + "}" : n
            }).join("")
        }, e.i = function(t, n) {
            "string" == typeof t && (t = [
                [null, t, ""]
            ]);
            for (var r = {}, o = 0; o < this.length; o++) {
                var i = this[o][0];
                "number" == typeof i && (r[i] = !0)
            }
            for (o = 0; o < t.length; o++) {
                var a = t[o];
                "number" == typeof a[0] && r[a[0]] || (n && !a[2] ? a[2] = n : n && (a[2] = "(" + a[2] + ") and (" + n + ")"), e.push(a))
            }
        }, e
    }
}, function(t, e, n) {
    var r = {},
        o = function(t) {
            var e;
            return function() {
                return void 0 === e && (e = t.apply(this, arguments)), e
            }
        }(function() {
            return window && document && document.all && !window.atob
        }),
        i = function(t) {
            var e = {};
            return function(t, n) {
                if ("function" == typeof t) return t();
                if (void 0 === e[t]) {
                    var r = function(t, e) {
                        return e ? e.querySelector(t) : document.querySelector(t)
                    }.call(this, t, n);
                    if (window.HTMLIFrameElement && r instanceof window.HTMLIFrameElement) try {
                        r = r.contentDocument.head
                    } catch (t) {
                        r = null
                    }
                    e[t] = r
                }
                return e[t]
            }
        }(),
        a = null,
        s = 0,
        u = [],
        c = n(23);

    function l(t, e) {
        for (var n = 0; n < t.length; n++) {
            var o = t[n],
                i = r[o.id];
            if (i) {
                i.refs++;
                for (var a = 0; a < i.parts.length; a++) i.parts[a](o.parts[a]);
                for (; a < o.parts.length; a++) i.parts.push(g(o.parts[a], e))
            } else {
                var s = [];
                for (a = 0; a < o.parts.length; a++) s.push(g(o.parts[a], e));
                r[o.id] = {
                    id: o.id,
                    refs: 1,
                    parts: s
                }
            }
        }
    }

    function f(t, e) {
        for (var n = [], r = {}, o = 0; o < t.length; o++) {
            var i = t[o],
                a = e.base ? i[0] + e.base : i[0],
                s = {
                    css: i[1],
                    media: i[2],
                    sourceMap: i[3]
                };
            r[a] ? r[a].parts.push(s) : n.push(r[a] = {
                id: a,
                parts: [s]
            })
        }
        return n
    }

    function h(t, e) {
        var n = i(t.insertInto);
        if (!n) throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
        var r = u[u.length - 1];
        if ("top" === t.insertAt) r ? r.nextSibling ? n.insertBefore(e, r.nextSibling) : n.appendChild(e) : n.insertBefore(e, n.firstChild), u.push(e);
        else if ("bottom" === t.insertAt) n.appendChild(e);
        else {
            if ("object" != typeof t.insertAt || !t.insertAt.before) throw new Error("[Style Loader]\n\n Invalid value for parameter 'insertAt' ('options.insertAt') found.\n Must be 'top', 'bottom', or Object.\n (https://github.com/webpack-contrib/style-loader#insertat)\n");
            var o = i(t.insertAt.before, n);
            n.insertBefore(e, o)
        }
    }

    function d(t) {
        if (null === t.parentNode) return !1;
        t.parentNode.removeChild(t);
        var e = u.indexOf(t);
        e >= 0 && u.splice(e, 1)
    }

    function p(t) {
        var e = document.createElement("style");
        if (void 0 === t.attrs.type && (t.attrs.type = "text/css"), void 0 === t.attrs.nonce) {
            var r = function() {
                0;
                return n.nc
            }();
            r && (t.attrs.nonce = r)
        }
        return y(e, t.attrs), h(t, e), e
    }

    function y(t, e) {
        Object.keys(e).forEach(function(n) {
            t.setAttribute(n, e[n])
        })
    }

    function g(t, e) {
        var n, r, o, i;
        if (e.transform && t.css) {
            if (!(i = "function" == typeof e.transform ? e.transform(t.css) : e.transform.default(t.css))) return function() {};
            t.css = i
        }
        if (e.singleton) {
            var u = s++;
            n = a || (a = p(e)), r = b.bind(null, n, u, !1), o = b.bind(null, n, u, !0)
        } else t.sourceMap && "function" == typeof URL && "function" == typeof URL.createObjectURL && "function" == typeof URL.revokeObjectURL && "function" == typeof Blob && "function" == typeof btoa ? (n = function(t) {
            var e = document.createElement("link");
            return void 0 === t.attrs.type && (t.attrs.type = "text/css"), t.attrs.rel = "stylesheet", y(e, t.attrs), h(t, e), e
        }(e), r = function(t, e, n) {
            var r = n.css,
                o = n.sourceMap,
                i = void 0 === e.convertToAbsoluteUrls && o;
            (e.convertToAbsoluteUrls || i) && (r = c(r));
            o && (r += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(o)))) + " */");
            var a = new Blob([r], {
                    type: "text/css"
                }),
                s = t.href;
            t.href = URL.createObjectURL(a), s && URL.revokeObjectURL(s)
        }.bind(null, n, e), o = function() {
            d(n), n.href && URL.revokeObjectURL(n.href)
        }) : (n = p(e), r = function(t, e) {
            var n = e.css,
                r = e.media;
            r && t.setAttribute("media", r);
            if (t.styleSheet) t.styleSheet.cssText = n;
            else {
                for (; t.firstChild;) t.removeChild(t.firstChild);
                t.appendChild(document.createTextNode(n))
            }
        }.bind(null, n), o = function() {
            d(n)
        });
        return r(t),
            function(e) {
                if (e) {
                    if (e.css === t.css && e.media === t.media && e.sourceMap === t.sourceMap) return;
                    r(t = e)
                } else o()
            }
    }
    t.exports = function(t, e) {
        if ("undefined" != typeof DEBUG && DEBUG && "object" != typeof document) throw new Error("The style-loader cannot be used in a non-browser environment");
        (e = e || {}).attrs = "object" == typeof e.attrs ? e.attrs : {}, e.singleton || "boolean" == typeof e.singleton || (e.singleton = o()), e.insertInto || (e.insertInto = "head"), e.insertAt || (e.insertAt = "bottom");
        var n = f(t, e);
        return l(n, e),
            function(t) {
                for (var o = [], i = 0; i < n.length; i++) {
                    var a = n[i];
                    (s = r[a.id]).refs--, o.push(s)
                }
                t && l(f(t, e), e);
                for (i = 0; i < o.length; i++) {
                    var s;
                    if (0 === (s = o[i]).refs) {
                        for (var u = 0; u < s.parts.length; u++) s.parts[u]();
                        delete r[s.id]
                    }
                }
            }
    };
    var v = function() {
        var t = [];
        return function(e, n) {
            return t[e] = n, t.filter(Boolean).join("\n")
        }
    }();

    function b(t, e, n, r) {
        var o = n ? "" : r.css;
        if (t.styleSheet) t.styleSheet.cssText = v(e, o);
        else {
            var i = document.createTextNode(o),
                a = t.childNodes;
            a[e] && t.removeChild(a[e]), a.length ? t.insertBefore(i, a[e]) : t.appendChild(i)
        }
    }
}, function(t, e) {
    t.exports = function(t) {
        var e = "undefined" != typeof window && window.location;
        if (!e) throw new Error("fixUrls requires window.location");
        if (!t || "string" != typeof t) return t;
        var n = e.protocol + "//" + e.host,
            r = n + e.pathname.replace(/\/[^\/]*$/, "/");
        return t.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(t, e) {
            var o, i = e.trim().replace(/^"(.*)"$/, function(t, e) {
                return e
            }).replace(/^'(.*)'$/, function(t, e) {
                return e
            });
            return /^(#|data:|http:\/\/|https:\/\/|file:\/\/\/|\s*$)/i.test(i) ? t : (o = 0 === i.indexOf("//") ? i : 0 === i.indexOf("/") ? n + i : r + i.replace(/^\.\//, ""), "url(" + JSON.stringify(o) + ")")
        })
    }
}]);
//# sourceMappingURL=obi-launcher.bundle.js.map