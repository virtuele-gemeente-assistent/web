document.querySelector("#reset-button").addEventListener("click", (e) => {
    window.sessionStorage.clear();
    window.location.reload();
});